@extends('frontend.layouts.master')
@section('title', 'Browse')
@section('content')
@section('nav_Browse', 'active')

<!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css" />-->


<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">
                <div class="rf-form_list_block">
                    <ul style="margin-bottom:0;">

                        <li>
                            <div class="rf-item" data-num="1" data-rel="rf_block1_0">
                                <div id="rfm1" class="rf-inactive">Browse by Disease</div>
                            </div>
                        </li>
                        <li>
                            <div class="rf-item" data-num="2" data-rel="rf_block2_0">
                                <div id="rfm2" class="rf-inactive">Browse by Gene</div>
                            </div>
                        </li>
                        <li>
                            <div class="rf-item" data-num="3" data-rel="rf_block3_0">
                                <div id="rfm3" class="rf-inactive">Browse by Variant</div>
                            </div>
                        </li>
                        <li>
                            <div class="rf-item" data-num="4" data-rel="rf_block4_0">
                                <div id="rfm4" class="rf-inactive">Browse by Case</div>
                            </div>
                        </li>
                    </ul>
                </div>



                <!-- block 1 -->
                <?php
                $i=1;
                $b1_page=0;
                ?>
                <div class="container_program rf_block1_0">
                    <table class="table table-bordered table-hover table-condensed" id="example">

                        <tbody>
                            <tr>
                                <td style="text-align:center;background-color:#4d6989;">

                                    <form action="{{url('Browse_disease')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="Long QT">
                                        <button type="submit" name="button" style="color:white; height:3.6vh;">Long QT syndrome (long QT)</button>
                                    </form>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align:center;background-color:#566457;">
                                    <form action="{{url('Browse_disease')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="BrS">
                                        <button type="submit" name="button" style="color:white; height:3.6vh;">
                                            Brugada Syndrome (BrS) </button>
                                    </form>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align:center;background-color:#ABC3BF;">
                                    <form action="{{url('Browse_disease')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="CPVT">
                                        <button type="submit" name="button" style="color:white; height:3.6vh;">
                                            Catecholaminergic Polymorphic Ventricular Tachycardia (CPVT) </button>
                                    </form>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align:center;background-color:#CAB44F;">
                                    <form action="{{url('Browse_disease')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="ARVC">
                                        <button type="submit" name="button" style="color:white; height:3.6vh;">
                                            Arrhythmogenic Right Ventricular Cardiomyopathy (ARVC)</button>
                                    </form>
                                </td>

                            </tr>
                            <tr>
                                <td style="text-align:center;background-color:#8F342F;">
                                    <form action="{{url('Browse_disease')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value="HCM">
                                        <button type="submit" name="button" style="color:white; height:3.6vh;">
                                            Hypertrophic Cardiomyopathy (HCM)</button>
                                    </form>
                                </td>

                            </tr>
                        </tbody>
                    </table>




                    <?php /*
  @foreach($petani as $key => $data)
      <tr>
        <th>{{$data->Depth}}</th>

        <th>{{$data->TaiwanBioBank}}</th>
        <th>{{$data->Barcode}}</th>
      </tr>
  @endforeach
  */ ?>
                </div>
                <!-- block 2 -->
                <?php
                $i=2;
                $b2_page=0;
                ?>
                <div class="container_program rf_block2_0">
                    <table class="table table-bordered table-hover table-condensed" id="example1">
                        <thead>
                            <tr>
                                <th title="Field #1" style="width:20%;background-color:#CAB44F;">Gene</th>
                                <th title="Field #2" style="width:40%;background-color:#CAB44F;">Genomic location of Variant</th>
                                <th title="Field #3" style="width:20%;background-color:#CAB44F;">SNP ID</th>
                                <th title="Field #4" style="width:20%;background-color:#CAB44F;">Case Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($genelist as $key => $data)
                            <?php $a = explode("&",$data->annotation);
                                    $a1 = $a[2];
                                ?>
                            <tr>
                                <th style="font-style:italic;">{{$data->gene}}</th>
                                <th>
                                    <form action="{{url('variants')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$data->variants}}>
                                        <button type="submit" name="button">{{$data->variants}}</button>
                                    </form>
                                </th>
                                <th>
                                    <?php if($a1 == "NA"){
                                      ?>

                                    <?php
                                  }
                                  else{
                                    ?>
                                    <a href="https://www.ncbi.nlm.nih.gov/snp/{{$a1}}">{{$a1}}</a></th>
                                <?php
                                  }
                                  ?>
                                <th style="margin-left: 3px">{{$data->number}}</th>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>

                </div>
                <!-- block 3 -->
                <?php
                $i=3;
                $b3_page=0;
                ?>
                <div class="container_program rf_block3_0">
                    <table class="table table-bordered table-hover table-condensed" id="example3">
                        <thead>
                            <tr>
                                <th title="Field #1" style="width:15%;background-color:#CAB44F;">Gene</th>
                                <th title="Field #1" style="width:30%;background-color:#CAB44F;">Genomic location of Variant</th>
                                <th title="Field #2" style="width:15%;background-color:#CAB44F;">SNP ID</th>
                                <th title="Field #3" style="width:20%;background-color:#CAB44F;">ACMG classification</th>
                                <th title="Field #3" style="width:20%;background-color:#CAB44F;">Case</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($variantslist as $key => $data)
                            <?php $a = explode("&",$data->annotation);
                                      $a1 = $a[2];
                                  ?>
                            <tr>
                                <th style="font-style:italic;">{{$data->gene}}</th>
                                <th>
                                    <form action="{{url('variants')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$data->variants}}>
                                        <button type="submit" name="button">{{$data->variants}}</button>
                                    </form>
                                </th>
                                <th>
                                    <?php if($a1 == "NA"){
                                      ?>

                                    <?php
                              }
                              else{
                                ?>
                                    <a href="https://www.ncbi.nlm.nih.gov/snp/{{$a1}}">{{$a1}}</a></th>
                                <?php
                              }
                              ?>
                                <th>
                                    <?php $d = explode(",",$data->ACMG_classify);
                                          echo $d[0];
                                    ?>
                                </th>
                                <th>
                                    <?php $c = explode(",",$data->alley_name);
                                        ?>
                                    @foreach ($c as $key => $datadata)
                                    <?php
                                        $c1 = explode("-",$datadata);
                                        $c2 = $c1[1]; #NTUH start = SUD-103

                                        if($c2<103) {
                                        ?>
                                    <form action="{{url('Browse_case')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$datadata}}>
                                        <button type="submit" name="button">{{$datadata}}</button>
                                    </form>
                                    <?php
                                    }
                                    else{
                                    ?>
                                    {{$datadata}}<br>
                                    <?php
                                    }
                                    ?>
                                    @endforeach
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <!-- block 4 -->
                <?php
                $i=4;
                $b4_page=0;
                ?>
                <div class="container_program rf_block4_0">
                    <table class="table table-bordered table-hover table-condensed" id="example4">
                        <thead>
                            <tr>
                                <th title="Field #1" style="width:20%;background-color:#CAB44F;">Case</th>
                                <th title="Field #2" style="width:20%;background-color:#CAB44F;">Disease of Cardiology</th>
                                <th title="Field #3" style="width:20%;background-color:#CAB44F;">Genes</th>
                                <th title="Field #3" style="width:40%;background-color:#CAB44F;">Variants</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($caselist as $key => $data)
                            <tr>
                                <th>
                                    <?php
                                        $c1 = explode("-",$data->alley_name);
                                        $c2 = $c1[1]; #NTUH start = SUD-103

                                        if($c2<103) {
                                        ?>
                                    <form action="{{url('Browse_case')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$data->alley_name}}>
                                        <button type="submit" name="button">{{$data->alley_name}}</button>
                                    </form>
                                    <?php
                                    }
                                    else{
                                    ?>
                                    {{$data->alley_name}}
                                    <?php
                                    }
                                    ?>
                                </th>
                                <th>{{$data->Disease}}</th>
                                <th style="font-style:italic;">
                                    <?php $g = explode(",",$data->geness);
                                        ?>
                                    @foreach ($g as $key => $datagg)
                                    {{$datagg}}<br>
                                    @endforeach
                                </th>
                                <th>
                                    <?php $v = explode(",",$data->variantss);
                                        ?>
                                    @foreach ($v as $key => $datass)
                                    <form action="{{url('variants')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$datass}}>
                                        <button type="submit" name="button">{{$datass}}</button>
                                    </form>
                                    @endforeach
                                </th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
</section>
<script>
    var i = 1;
    var current_page = 1;
    var block_page = [0, <?php echo $b1_page.",".$b2_page.",".$b3_page.",".$b4_page;?>];

    $(document).ready(function() {
        $(".container_program").hide();
        $("#rfm" + i).removeClass("rf-inactive").addClass("rf-active");
        $(".rf_block" + i + "_0").fadeIn(10, "swing");
    });

    $(".rf-item").click(function() {
        current_page = 0;
        $("#rfm" + i).removeClass("rf-active").addClass("rf-inactive");

        i = $(this).data('num');
        $("#rfm" + i).removeClass("rf-inactive").addClass("rf-active");


        $(".container_program").fadeOut(10, "swing");
        $('.' + $(this).data('rel')).fadeIn(10, "swing");
    });
</script>
<!--
<script type="text/javascript" src="jquery/jquery.min.js"></script>
<script type="text/javascript" src="js/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
    $('#example').dataTable({
        "scrollX": true
    });
</script>
-->
@endsection