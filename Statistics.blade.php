@extends('frontend.layouts.master')
@section('title', 'Statistics')
@section('content')
@section('nav_Statistics', 'active')


<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">
                <div class="container_program">
                    <div class="st_plot_area">
                        <h5>&nbsp;Number of patients/identified variants in sudden cardiac diseases</h5>
                        <br>
                        <canvas id="BarPlot1" width="400" height="200"></canvas>
                    </div>
                    <div class="st_plot_area" style="height:1000px;">
                        <h5>&nbsp;Number of identified variants in patients</h5>
                        <br>
                        <div id="plot9" style="height:800px"></div>
                    </div>
                    <br>
                    <br>
                    <!--
                    <div class="st_plot_area">
                        <h5>&nbsp;Number of identified variants in patients</h5>
                        <br>
                        <canvas id="BarPlot2" width="400" height="600"></canvas>
                    </div>
                  -->
                    <div class="st_plot_area">
                        <h5>&nbsp;Number of identified variants in different reported classifications</h5>
                        <br>
                        <canvas id="BarPlot3" width="400" height="150"></canvas>
                    </div>
                    <div class="st_plot_area">
                        <h5>&nbsp;The percentage of patients carrying sudden cardiac diseases-related genetic variations in diseases</h5>
                        <br>
                        <div class="rf-form_list_block4">
                            <ul style="margin-bottom:0;">

                                <li>
                                    <div class="rf-item4" data-num="1" data-rel="rf_block_four1_0">
                                        <div id="rfm_plot41" class="rf-inactive4">All</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="2" data-rel="rf_block_four2_0">
                                        <div id="rfm_plot42" class="rf-inactive4">Long QT</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="3" data-rel="rf_block_four3_0">
                                        <div id="rfm_plot43" class="rf-inactive4">BrS</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="4" data-rel="rf_block_four4_0">
                                        <div id="rfm_plot44" class="rf-inactive4">CPVT</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="5" data-rel="rf_block_four5_0">
                                        <div id="rfm_plot45" class="rf-inactive4">ARVC</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="6" data-rel="rf_block_four6_0">
                                        <div id="rfm_plot46" class="rf-inactive4">HCM</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item4" data-num="7" data-rel="rf_block_four7_0">
                                        <div id="rfm_plot47" class="rf-inactive4">Other</div>
                                    </div>
                                </li>
                            </ul>

                            <?php
                        $j=1;
                        $j1_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four1_0">
                                <div id="plot4_1" style="height:400px"></div>
                            </div>
                            <?php
                        $j=2;
                        $j2_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four2_0">
                                <div id="plot4_2" style="height:400px"></div>
                            </div>
                            <?php
                        $j=3;
                        $j3_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four3_0">
                                <div id="plot4_3" style="height:400px"></div>
                            </div>
                            <?php
                        $j=4;
                        $j4_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four4_0">
                                <div id="plot4_4" style="height:400px"></div>
                            </div>
                            <?php
                        $j=5;
                        $j5_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four5_0">
                                <div id="plot4_5" style="height:400px"></div>
                            </div>
                            <?php
                        $j=6;
                        $j6_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four6_0">
                                <div id="plot4_6" style="height:400px"></div>
                            </div>
                            <?php
                        $j=7;
                        $j7_page=0;
                        ?>
                            <div class="container_program_4 rf_block_four7_0">
                                <div id="plot4_7" style="height:400px"></div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="st_plot_area">
                        <h5>&nbsp;Number of patients carrying sudden cardiac diseases-related genetic variations</h5>
                        <br>
                        <canvas id="BarPlot4" width="400" height="200"></canvas>
                    </div>

                    <div class="st_plot_area">
                        <h5>&nbsp;Number of patients in different statuses</h5>
                        <br>
                        <div class="rf-form_list_block5">
                            <ul style="margin-bottom:0;">

                                <li>
                                    <div class="rf-item5" data-num="1" data-rel="rf_block_five1_0">
                                        <div id="rfm_plot51" class="rf-inactive5">Sexual</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item5" data-num="2" data-rel="rf_block_five2_0">
                                        <div id="rfm_plot52" class="rf-inactive5">Aging</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item5" data-num="3" data-rel="rf_block_five3_0">
                                        <div id="rfm_plot53" class="rf-inactive5">Smoke or not</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item5" data-num="4" data-rel="rf_block_five4_0">
                                        <div id="rfm_plot54" class="rf-inactive5">Alcohol consumption or not</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item5" data-num="5" data-rel="rf_block_five5_0">
                                        <div id="rfm_plot55" class="rf-inactive5">Red face or not</div>
                                    </div>
                                </li>
                            </ul>
                            <?php
                        $i=1;
                        $b1_page=0;
                        ?>
                            <div class="container_program_5 rf_block_five1_0">
                                <div id="plot5_1" style="height:400px"></div>
                            </div>
                            <?php
                        $i=2;
                        $b2_page=0;
                        ?>
                            <div class="container_program_5 rf_block_five2_0">
                                <div id="plot5_2" style="height:400px"></div>
                            </div>
                            <?php
                        $i=3;
                        $b3_page=0;
                        ?>
                            <div class="container_program_5 rf_block_five3_0">
                                <div id="plot5_3" style="height:400px"></div>
                            </div>
                            <?php
                        $i=4;
                        $b4_page=0;
                        ?>
                            <div class="container_program_5 rf_block_five4_0">
                                <div id="plot5_4" style="height:400px"></div>
                            </div>
                            <?php
                        $i=5;
                        $b5_page=0;
                        ?>
                            <div class="container_program_5 rf_block_five5_0">
                                <div id="plot5_5" style="height:400px"></div>
                            </div>
                        </div>
                    </div>
                  -->
                    <div class="st_plot_area">
                        <h5>&nbsp; The percentage of patients carrying sudden cardiac diseases-related genetic variations in different statuses</h5>
                        <br>
                        <div class="rf-form_list_block6">
                            <ul style="margin-bottom:0;">

                                <li>
                                    <div class="rf-item6" data-num="1" data-rel="rf_block1_6">
                                        <div id="rfm_plot61" class="rf-inactive6">Sexual</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item6" data-num="2" data-rel="rf_block2_6">
                                        <div id="rfm_plot62" class="rf-inactive6">Aging</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item6" data-num="3" data-rel="rf_block3_6">
                                        <div id="rfm_plot63" class="rf-inactive6">Smoke or not</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item6" data-num="4" data-rel="rf_block4_6">
                                        <div id="rfm_plot64" class="rf-inactive6">Alcohol consumption or not</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item6" data-num="5" data-rel="rf_block5_6">
                                        <div id="rfm_plot65" class="rf-inactive6">Red face or not</div>
                                    </div>
                                </li>
                            </ul>
                            <?php
                        $k=1;
                        $a1_page=0;
                        ?>
                            <div class="container_program_6 rf_block1_6">
                                <div id="plot6_1" style="height:400px"></div>
                            </div>
                            <?php
                        $k=2;
                        $a2_page=0;
                        ?>
                            <div class="container_program_6 rf_block2_6">
                                <div id="plot6_2" style="height:400px"></div>
                            </div>
                            <?php
                        $k=3;
                        $a3_page=0;
                        ?>
                            <div class="container_program_6 rf_block3_6">
                                <div id="plot6_3" style="height:400px"></div>
                            </div>
                            <?php
                        $k=4;
                        $a4_page=0;
                        ?>
                            <div class="container_program_6 rf_block4_6">
                                <div id="plot6_4" style="height:400px"></div>
                            </div>
                            <?php
                        $k=5;
                        $a5_page=0;
                        ?>
                            <div class="container_program_6 rf_block5_6">
                                <div id="plot6_5" style="height:400px"></div>
                            </div>
                        </div>
                    </div>
                    <div class="st_plot_area">
                        <h5>&nbsp;Characteristics of EKG data in different sudden cardiac diseases </h5>
                        <br>
                        <div id="plot7_1" style="height:400px"></div>
                    </div>
                    <div class="st_plot_area">
                        <h5>&nbsp;Characteristics of Echo data in different sudden cardiac diseases</h5>
                        <br>
                        <div class="rf-form_list_block8">
                            <ul style="margin-bottom:0;">

                                <li>
                                    <div class="rf-item8" data-num="1" data-rel="rf_block1_8">
                                        <div id="rfm_plot81" class="rf-inactive8">LA/LV</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item8" data-num="2" data-rel="rf_block2_8">
                                        <div id="rfm_plot82" class="rf-inactive8">RVSP</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item8" data-num="3" data-rel="rf_block3_8">
                                        <div id="rfm_plot83" class="rf-inactive8">LVEF</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item8" data-num="4" data-rel="rf_block4_8">
                                        <div id="rfm_plot84" class="rf-inactive8">DT deceleration time</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item8" data-num="5" data-rel="rf_block5_8">
                                        <div id="rfm_plot85" class="rf-inactive8">E/A wave</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="rf-item8" data-num="6" data-rel="rf_block6_8">
                                        <div id="rfm_plot86" class="rf-inactive8">EA division</div>
                                    </div>
                                </li>
                            </ul>
                            <?php
                        $s=1;
                        $s1_page=0;
                        ?>
                            <div class="container_program_8 rf_block1_8">
                                <div id="plot8_1" style="height:400px"></div>
                            </div>
                            <?php
                        $s=2;
                        $s2_page=0;
                        ?>
                            <div class="container_program_8 rf_block2_8">
                                <div id="plot8_2" style="height:400px"></div>
                            </div>
                            <?php
                        $s=3;
                        $s3_page=0;
                        ?>
                            <div class="container_program_8 rf_block3_8">
                                <div id="plot8_3" style="height:400px"></div>
                            </div>
                            <?php
                        $s=4;
                        $s4_page=0;
                        ?>
                            <div class="container_program_8 rf_block4_8">
                                <div id="plot8_4" style="height:400px"></div>
                            </div>
                            <?php
                        $s=5;
                        $s5_page=0;
                        ?>
                            <div class="container_program_8 rf_block5_8">
                                <div id="plot8_5" style="height:400px"></div>
                            </div>
                            <?php
                        $s=6;
                        $s6_page=0;
                        ?>
                            <div class="container_program_8 rf_block6_8">
                                <div id="plot8_6" style="height:400px"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script type="text/javascript">
        //plot 1
        var ctx = document.getElementById('BarPlot1');
        var BarPlot1 = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: ['Long QT', 'BrS', 'CPVT', 'ARVC', 'HCM', 'Other'],
                datasets: [{
                    label: "patients",
                    backgroundColor: '#566457',
                    data: [<?php echo $count_case_longQT_bar;?>,
                        <?php echo $count_case_BrS_bar; ?>,
                        <?php echo $count_case_CPVT_bar; ?>,
                        <?php echo $count_case_ARVC_bar; ?>,
                        <?php echo $count_case_HCM_bar; ?>,
                        <?php echo $count_case_o_bar; ?>
                    ]
                }, {
                    label: "variants",
                    backgroundColor: '#ABC3BF',
                    data: [<?php echo $count_var_longQT_bar;?>,
                        <?php echo $count_var_BrS_bar; ?>,
                        <?php echo $count_var_CPVT_bar; ?>,
                        <?php echo $count_var_ARVC_bar; ?>,
                        <?php echo $count_var_HCM_bar; ?>,
                        <?php echo $count_var_o_bar; ?>
                    ]
                }]

            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 0,
                            stepSize: 1
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }

                    }]
                },
            }
        });


        //plot 2
        /*
        var ctx2 = document.getElementById('BarPlot2');
        var BarPlot2 = new Chart(ctx2, {
            type: 'horizontalBar',
            data: {
                labels: ["g.chr1:237765380A>G|c.4652A>G|p.N1551S", "g.chr11:47353740G>A|c.3697C>T|p.Q1233X", "g.chr6:7569480A>T|c.1481A>T|p.Y494F", "g.chr3:38608067C>T|c.3673G>A|p.E1225K", "g.chr6:7583238C>A|c.5743C>A|p.R1915S",
                    "g.chr6:7580187G>C|c.3764G>C|p.R1255T", "g.chr6:7574952A>G|c.2360A>G|p.Y787C", "g.chr3:38592513C>T|c.5350G>A|p.E1784K", "g.chr1:237777474A>C|c.5046A>C|p.E1682D", "g.chr10:18828261C>T|c.1591C>T|p.R531C",
                    "g.chr1:237778084G>A|c.5656G>A|p.G1886S", "g.chr6:7580346G>A|c.3923G>A|p.R1308Q", "g.chr12:32974441G>C|c.1862C>G|p.P621R", "g.chr6:7581636G>A|c.5213G>A|p.R1738Q", "g.chr3:38616915G>A|c.3539C>T|p.A1180V",
                    "g.chr3:38604001T>C|c.3706A>G|p.T1236A", "g.chr11:47370063A>C|c.684T>G|p.D228E", "g.chr3:38622757G>A|c.2893C>T|p.R965C", "g.chr3:38655249T>C|c.688A>G|p.I230V", "g.chr14:23883054C>G|c.5704G>C|p.E1902Q",
                    "g.chr7:150647022G>A|c.2632C>T|p.R878C", "g.chr1:237754226C>T|c.4094C>T|p.A1365V", "g.chr11:47364231G>A|c.1522C>T|p.Q508X", "g.chr12:2800220A>G|c.6377A>G|p.N2126S", "g.chr11:2591955G>A|c.575G>A|p.R192H",
                    "g.chr3:38598739C>A|c.4282G>T|p.A1428S", "g.chr6:7584294A>T|c.6799A>T|p.T2267S", "g.chr3:38640439C>A|c.1993G>T|p.A665S", "g.chr6:7568727T>G|c.1324T>G|p.S442A", "g.chr3:38616912A>G|c.3380T>C|p.V1127A",
                    "g.chr3:38620946G>A|c.3266C>T|p.P1089L", "g.chr10:18828169G>A|c.1499G>A|p.G500D", "g.chr6:7559521G>T|c.485G>T|p.R162L", "g.chr10:18828605A>C|c.1935A>C|p.K645N", "g.chr1:237778042G>A|c.5614G>A|p.D1872N",
                    "g.chr6:7556049A>G|c.269A>G|p.Q90R", "g.chr12:33021934A>G|c.1097T>C|p.L366P", "g.chr7:150644856G>A|c.2803C>T|p.P935S", "g.chr6:7584484T>G|c.6989T>G|p.L2330R", "g.chr6:7585411G>A|c.7916G>A|p.R2639Q",
                    "g.chr21:35821680C>T|c.253G>A|p.D85N", "g.chr11:47371336G>A|c.643C>T|p.R215C", "g.chr11:47367848C>T|c.1000G>A|p.E334K", "g.chr1:237666602C>T|c.2410C>T|p.L804F", "g.chr3:38601865C>T|c.4018G>A|p.V1340I",
                    "g.chr11:47356737G>C|c.2761C>G|p.Q921E", "g.chr11:47372978C>T|c.104G>A|p.R35Q", "g.chr6:7580913G>A|c.4490G>A|p.R1497Q", "g.chr11:47364234C>T|c.1519G>A|p.G507R", "g.chr6:7583998G>C|c.6503G>C|p.S2168T",
                    "g.chr1:237948232G>A|c.13220G>A|p.S4407N", "g.chr12:32974457G>A|c.1846C>T|p.Q616X", "g.chr12:2788862G>A|c.5344G>A|p.A1782T", "g.chr6:7585539C>G|c.8044C>G|p.Q2682E", "g.chr14:23902865G>A|c.77C>T|p.A26V",
                    "g.chr14:23892874T>C|c.2981A>G|p.K994R", "g.chr3:38655260G>A|c.677C>T|p.A226V", "g.chr11:47355161G>A|c.3137C>T|p.T1046M", "g.chr1:237664082T>A|c.2275T>A|p.L759I", "g.chr3:38645534A>C|c.1559T>G|p.M520R",
                    "g.chr3:38640439C>T|c.1993G>A|p.A665T", "g.chr11:47355475G>C|c.2992C>G|p.Q998E", "g.chr12:2788847C>T|c.5329C>T|p.R1777C", "g.chr10:18795465C>A|c.659C>A|p.P220H"
                ],
                datasets: [{
                    label: "patients",
                    backgroundColor: '#CAB44F',
                    data: [
                        <?php #echo $count_mut_1_;?>,
                        <?php #echo $count_mut_2_; ?>,
                        <?php #echo $count_mut_4_; ?>,
                        <?php #echo $count_mut_6_; ?>,
                        <?php #echo $count_mut_8_; ?>,
                        <?php #echo $count_mut_10_; ?>,
                        <?php #echo $count_mut_11_; ?>,
                        <?php #echo $count_mut_12_; ?>,
                        <?php #echo $count_mut_15_; ?>,
                        <?php #echo $count_mut_16_; ?>,
                        <?php #echo $count_mut_17_; ?>,
                        <?php #echo $count_mut_18_; ?>,
                        <?php #echo $count_mut_23_; ?>,
                        <?php #echo $count_mut_24_; ?>,
                        <?php #echo $count_mut_25_; ?>,
                        <?php #echo $count_mut_26_; ?>,
                        <?php #echo $count_mut_28_; ?>,
                        <?php #echo $count_mut_29_; ?>,
                        <?php #echo $count_mut_30_; ?>,
                        <?php #echo $count_mut_33_; ?>,
                        <?php #echo $count_mut_37_; ?>,
                        <?php #echo $count_mut_38_; ?>,
                        <?php #echo $count_mut_39_; ?>,
                        <?php #echo $count_mut_40_; ?>,
                        <?php #echo $count_mut_41_; ?>,
                        <?php #echo $count_mut_42_; ?>,
                        <?php #echo $count_mut_43_; ?>,
                        <?php #echo $count_mut_44_; ?>,
                        <?php #echo $count_mut_48_; ?>,
                        <?php #echo $count_mut_49_; ?>,
                        <?php #echo $count_mut_51_; ?>,
                        <?php #echo $count_mut_52_; ?>,
                        <?php #echo $count_mut_53_; ?>,
                        <?php #echo $count_mut_55_; ?>,
                        <?php #echo $count_mut_56_; ?>,
                        <?php #echo $count_mut_58_; ?>,
                        <?php #echo $count_mut_59_; ?>,
                        <?php #echo $count_mut_60_; ?>,
                        <?php #echo $count_mut_62_; ?>,
                        <?php #echo $count_mut_63_; ?>,
                        <?php #echo $count_mut_64_; ?>,
                        <?php #echo $count_mut_66_; ?>,
                        <?php #echo $count_mut_70_; ?>,
                        <?php #echo $count_mut_72_; ?>,
                        <?php #echo $count_mut_77_; ?>,
                        <?php #echo $count_mut_79_; ?>,
                        <?php #echo $count_mut_80_; ?>,
                        <?php #echo $count_mut_81_; ?>,
                        <?php #echo $count_mut_82_; ?>,
                        <?php #echo $count_mut_84_; ?>,
                        <?php #echo $count_mut_85_; ?>,
                        <?php #echo $count_mut_88_; ?>,
                        <?php #echo $count_mut_89_; ?>,
                        <?php #echo $count_mut_90_; ?>,
                        <?php #echo $count_mut_92_; ?>,
                        <?php #echo $count_mut_93_; ?>,
                        <?php #echo $count_mut_94_; ?>,
                        <?php #echo $count_mut_101_; ?>,
                        <?php #echo $count_mut_102_; ?>,
                        <?php #echo $count_mut_103_; ?>,
                        <?php #echo $count_mut_105_; ?>,
                        <?php #echo $count_mut_107_; ?>,
                        <?php #echo $count_mut_111_; ?>,
                        <?php #echo $count_mut_112_; ?>

                    ]
                }]

            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }

                    }]
                },
            }
        });
        */
        //plot 3
        var ctx3 = document.getElementById('BarPlot3');
        var BarPlot3 = new Chart(ctx3, {
            type: 'horizontalBar',
            data: {
                labels: ['Pathogenic', 'Likely pathogenic', 'Uncertain Significance'],
                datasets: [{
                    label: "variants",
                    backgroundColor: '#464646',
                    data: [<?php echo $count_C_P_bar;?>,
                        <?php echo $count_C_LP_bar; ?>,
                        <?php echo $count_C_US_bar; ?>
                    ]
                }]

            },
            options: {
                scales: {
                    xAxes: [{
                        ticks: {
                            min: 0,
                        }
                    }],
                    yAxes: [{
                        barThickness: 40,
                        ticks: {
                            beginAtZero: true
                        }

                    }]
                },
            }
        });
    </script>

    <script src="js/incubator-echarts/dist/echarts.js"></script>
    <!-- plot 4 -->
    <script type="text/javascript">
        //4-1
        var myChart = echarts.init(document.getElementById('plot4_1'));
        var id1 = [
            <?php foreach ($count_mut_all as $key => $value) {
                  json_encode($value->countt);
              }?>
        ];
        var id2 = [
            <?php
                foreach ($count_mut_all as $key => $value) {
                  json_encode($count_all_ - $value->countt);
                }?>
        ];
        var transpose = function(A) {
            var arr = [];
            for (var i = 0; i < A.length; i++) {
                arr[i] = []
                for (var j = 0; j < A[0].length; j++) {
                    arr[i][j] = A[j][i]
                }
            }
            return arr
        };

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all as $key => $value) {
                              echo round(($value->countt / ($count_all_))*100);
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all as $key => $value) {
                              echo round((($count_all_ - $value->countt)/$count_all_)*100);
                              echo ",";
                            }?>]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-2
        var myChart = echarts.init(document.getElementById('plot4_2'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_longQT as $key => $value) {
                              echo round(($value->countt / ($count_all_longQT))*100);
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_longQT as $key => $value) {
                              echo round((($count_all_longQT - $value->countt)/$count_all_longQT)*100);
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-3
        var myChart = echarts.init(document.getElementById('plot4_3'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all_BrS as $key => $value) {
                      echo floor(($value->countt / ($count_all_BrS))*100);
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_BrS as $key => $value) {
                      echo round((($count_all_BrS - $value->countt)/$count_all_BrS)*100);
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-4
        var myChart = echarts.init(document.getElementById('plot4_4'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_CPVT as $key => $value) {
                              if($count_all_CPVT == 0){
                              echo 0;
                              }
                              else{
                                echo round(($value->countt / ($count_all_CPVT))*100);

                              }
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_CPVT as $key => $value) {
                          if($count_all_CPVT == 0){
                          echo 0;
                          }
                          else{
                      echo round((($count_all_CPVT - $value->countt)/$count_all_CPVT)*100);

                                                    }
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-5
        var myChart = echarts.init(document.getElementById('plot4_5'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all_ARVC as $key => $value) {
                      echo round(($value->countt / ($count_all_ARVC))*100);
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_ARVC as $key => $value) {
                      echo round((($count_all_ARVC - $value->countt)/$count_all_ARVC)*100);
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-6
        var myChart = echarts.init(document.getElementById('plot4_6'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all_HCM as $key => $value) {
                      echo round(($value->countt / ($count_all_HCM))*100);
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_HCM as $key => $value) {
                      echo round((($count_all_HCM - $value->countt)/$count_all_HCM)*100);
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //4-7
        var myChart = echarts.init(document.getElementById('plot4_7'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {

                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + transpose(id1) + '\n' + params[0].value + '%<br/>' +
                        params[1].seriesName + ' : ' + transpose(id2) + '\n' + params[1].value + '%';
                }
            },
            legend: {
                data: ['Mut', 'Wild-type']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_all as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [<?php foreach ($count_mut_all_other as $key => $value) {
                      echo round(($value->countt / ($count_all_other))*100);
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_all_other as $key => $value) {
                              echo round((($count_all_other - $value->countt)/$count_all_other)*100);

                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);


        var j = 1;
        var current_page = 1;
        var block_page = [0,
            <?php echo $j1_page.",".$j2_page.",".$j3_page.",".$j4_page.",".$j5_page.",".$j6_page.",".$j7_page;?>
        ];
        $(document).ready(function() {
            $(".container_program_4").hide();
            $("#rfm_plot4" + j).removeClass("rf-inactive4").addClass("rf-active4");
            $(".rf_block_four" + j + "_0").fadeIn(10, "swing");
        });

        $(".rf-item4").click(function() {
            current_page = 0;
            $("#rfm_plot4" + j).removeClass("rf-active4").addClass("rf-inactive4");
            $(".container_program_4").fadeOut(10, "swing");

            j = $(this).data('num');
            $("#rfm_plot4" + j).removeClass("rf-inactive4").addClass("rf-active4");

            $('.' + $(this).data('rel')).fadeIn(10, "swing");

        });
    </script>
    <script type="text/javascript">
        //5-1
        /*
        var myChart = echarts.init(document.getElementById('plot5_1'));

        var option = {
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['Mut - male', 'Wild-type - male', 'Mut - female', 'Wild-type - female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($D_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'Mut - male',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_gender_male as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - male',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(98,23,8,1)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_gender_female as $key => $value) {
                              echo $D_wp_male_ - $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Mut - female',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_gender_female as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - female',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(188,57,8,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_gender_female as $key => $value) {
                              echo $D_wp_female_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //5-2
        var myChart = echarts.init(document.getElementById('plot5_2'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['Mut - under 30', 'Wild-type - under 30', 'Mut - 30-60', 'Wild-type - 30-60', 'Mut - 60 up', 'Wild-type - 60 up']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($D_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'Mut - under 30',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_youngage as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - under 30',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(98,23,8,1)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_youngage as $key => $value) {
                              echo $D_wp_youngage_ - $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Mut - 30-60',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_midage as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - 30-60',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(188,57,8,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_midage as $key => $value) {
                              echo $D_wp_midage_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - 60 up',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(89,141,160,0.5)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_oldage as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - 60 up',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(246,170,28,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_oldage as $key => $value) {
                              echo $D_wp_oldage_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //5-3
        var myChart = echarts.init(document.getElementById('plot5_3'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['Mut - smoke', 'Wild-type - smoke', 'Mut - exsmoke', 'Wild-type - exsmoke', 'Mut - no smoke', 'Wild-type - no smoke']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_smoke as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($D_mut_smoke as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'Mut - smoke',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_smoke as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - smoke',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(98,23,8,1)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_smoke as $key => $value) {
                              echo $D_wp_smoke_ - $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Mut - exsmoke',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_exsmoke as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - exsmoke',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(188,57,8,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_exsmoke as $key => $value) {
                              echo $D_wp_exsmoke_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - no smoke',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(89,141,160,0.5)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_nosmoke as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - no smoke',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(246,170,28,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_nosmoke as $key => $value) {
                              echo $D_wp_nosmoke_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //5-4
        var myChart = echarts.init(document.getElementById('plot5_4'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['Mut - alcohol consumption', 'Wild-type - alcohol consumption', 'Mut - no alcohol consumption', 'Wild-type - no alcohol consumption']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_alchol as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($D_mut_alchol as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'Mut - alcohol consumption',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_alchol as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - alcohol consumption',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(98,23,8,1)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_alchol as $key => $value) {
                              echo $D_wp_alchol_ - $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Mut - no alcohol consumption',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_noalchol as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - no alcohol consumption',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(188,57,8,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_noalchol as $key => $value) {
                              echo $D_wp_noalchol_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //5-5
        var myChart = echarts.init(document.getElementById('plot5_5'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['Mut - red face', 'Wild-type - red face', 'Mut - no red face', 'Wild-type - no red face']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_redface as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($D_mut_redface as $key => $value) {
                              echo "'";
                              echo $value->DIAG;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'Mut - red face',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_redface as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - red face',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(98,23,8,1)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_redface as $key => $value) {
                              echo $D_wp_redface_ - $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Mut - no red face',
                    type: 'bar',
                    stack: 'mut',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [<?php foreach ($D_mut_noredface as $key => $value) {
                              echo $value->countt;
                              echo ",";
                            }?>]
                },
                {
                    name: 'Wild-type - no red face',
                    type: 'bar',
                    stack: 'wt',
                    itemStyle: {
                        normal: {
                            color: 'rgba(188,57,8,0.75)',
                            label: {
                                show: true
                            }
                        }
                    },
                    data: [
                        <?php foreach ($D_mut_noredface as $key => $value) {
                              echo $D_wp_noredface_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        var i = 1;
        var current_page = 1;
        var block_page = [0,
            <?php echo $b1_page.",".$b2_page.",".$b3_page.",".$b4_page.",".$b5_page;?>
        ];
        $(document).ready(function() {
            $(".container_program_5").hide();
            $("#rfm_plot5" + i).removeClass("rf-inactive5").addClass("rf-active5");
            $(".rf_block_five" + i + "_0").fadeIn(10, "swing");
        });

        $(".rf-item5").click(function() {
            current_page = 0;
            $("#rfm_plot5" + i).removeClass("rf-active5").addClass("rf-inactive5");
            $(".container_program_5").fadeOut(10, "swing");

            y = $(this).data('num');
            $("#rfm_plot5" + i).removeClass("rf-inactive5").addClass("rf-active5");

            $('.' + $(this).data('rel')).fadeIn(10, "swing");

        });
      */
    </script>
    <script type="text/javascript">
        //6-1
        var myChart = echarts.init(document.getElementById('plot6_1'));

        var option = {
            tooltip: {
                trigger: 'axis',
                formatter: function(params) {
                    /*
                      return params[0].name + '<br/>' +
                          params[0].seriesName + ' : ' + params[0].value + '\n(' + (params[0].value / 64 * 100).toFixed(0) + '%)<br/>' +
                          params[1].seriesName + ' : ' + params[1].value + '\n(' + (params[1].value / 64 * 100).toFixed(0) + '%)<br/>' +
                          params[2].seriesName + ' : ' + params[2].value + '\n(' + (params[2].value / 29 * 100).toFixed(0) + '%)<br/>' +
                          params[3].seriesName + ' : ' + params[3].value + '\n(' + (params[3].value / 29 * 100).toFixed(0) + '%)'
                    */
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n%<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n%<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n%<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n%'

                }
            },
            legend: {
                data: ['Mut - male', 'Wild-type - male', 'Mut - female', 'Wild-type - female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut - male',
                    type: 'bar',
                    stack: 'male',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo round(($value->countt / ($count_wp_male_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - male',
                    type: 'bar',
                    stack: 'male',
                    itemStyle: {
                        normal: {
                            color: 'rgba(110,110,110,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo round((($count_wp_male_ - $value->countt)/$count_wp_male_)*100);
                              #echo $count_wp_male_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - female',
                    type: 'bar',
                    stack: 'female',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_female as $key => $value) {
                              echo round(($value->countt / ($count_wp_female_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - female',
                    type: 'bar',
                    stack: 'female',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_female as $key => $value) {
                              echo round((($count_wp_female_ - $value->countt)/$count_wp_female_)*100);
                              #echo $count_wp_female_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //6-2
        var myChart = echarts.init(document.getElementById('plot6_2'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params) {
                    /*
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n(' + (params[0].value / 86 * 100).toFixed(0) + '%)<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n(' + (params[1].value / 86 * 100).toFixed(0) + '%)<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n(' + (params[2].value / 60 * 100).toFixed(0) + '%)<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n(' + (params[3].value / 60 * 100).toFixed(0) + '%)<br/>' +
                        params[4].seriesName + ' : ' + params[4].value + '\n(' + (params[4].value / 25 * 100).toFixed(0) + '%)<br/>' +
                        params[5].seriesName + ' : ' + params[5].value + '\n(' + (params[5].value / 25 * 100).toFixed(0) + '%)'
                        */
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n%<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n%<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n%<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n%<br/>' +
                        params[4].seriesName + ' : ' + params[4].value + '\n%<br/>' +
                        params[5].seriesName + ' : ' + params[5].value + '\n%'
                }
            },
            legend: {
                data: ['Mut - under 30', 'Wild-type - under 30', 'Mut - 30-60', 'Wild-type - 30-60', 'Mut - 60 up', 'Wild-type - 60 up']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_gender_male as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut - under 30',
                    type: 'bar',
                    stack: '30',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_youngage as $key => $value) {
                              echo round(($value->countt / ($count_wp_youngage_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - under 30',
                    type: 'bar',
                    stack: '30',
                    itemStyle: {
                        normal: {
                            color: 'rgba(80,80,80,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_youngage as $key => $value) {
                              echo round((($count_wp_youngage_ - $value->countt)/$count_wp_youngage_)*100);
                              #echo $count_wp_youngage_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - 30-60',
                    type: 'bar',
                    stack: '40',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_midage as $key => $value) {
                              echo round(($value->countt / ($count_wp_midage_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - 30-60',
                    type: 'bar',
                    stack: '40',
                    itemStyle: {
                        normal: {
                            color: 'rgba(110,110,110,0.7)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_midage as $key => $value) {
                              echo round((($count_wp_midage_ - $value->countt)/$count_wp_midage_)*100);
                              #echo $count_wp_midage_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - 60 up',
                    type: 'bar',
                    stack: '60',
                    itemStyle: {
                        normal: {
                            color: 'rgba(89,141,160,0.5)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_oldage as $key => $value) {
                              echo round(($value->countt / ($count_wp_oldage_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - 60 up',
                    type: 'bar',
                    stack: '60',
                    itemStyle: {
                        normal: {
                            color: 'rgba(140,140,140,0.5)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_oldage as $key => $value) {
                              echo round((($count_wp_oldage_ - $value->countt)/$count_wp_oldage_)*100);
                              #echo $count_wp_oldage_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //6-3
        var myChart = echarts.init(document.getElementById('plot6_3'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params) {
                    /*
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n(' + (params[0].value / 15 * 100).toFixed(0) + '%)<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n(' + (params[1].value / 15 * 100).toFixed(0) + '%)<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n(' + (params[2].value / 5 * 100).toFixed(0) + '%)<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n(' + (params[3].value / 5 * 100).toFixed(0) + '%)<br/>' +
                        params[4].seriesName + ' : ' + params[4].value + '\n(' + (params[4].value / 22 * 100).toFixed(0) + '%)<br/>' +
                        params[5].seriesName + ' : ' + params[5].value + '\n(' + (params[5].value / 22 * 100).toFixed(0) + '%)'
                        */
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n%<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n%<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n%<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n%<br/>' +
                        params[4].seriesName + ' : ' + params[4].value + '\n%<br/>' +
                        params[5].seriesName + ' : ' + params[5].value + '\n%'
                }
            },
            legend: {
                data: ['Mut - smoke', 'Wild-type - smoke', 'Mut - exsmoke', 'Wild-type - exsmoke', 'Mut - no smoke', 'Wild-type - no smoke']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_smoke as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_smoke as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut - smoke',
                    type: 'bar',
                    stack: 'smoke',
                    itemStyle: {
                        normal: {
                            color: '#022B3A',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_smoke as $key => $value) {
                              echo round(($value->countt / ($count_wp_smoke_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - smoke',
                    type: 'bar',
                    stack: 'smoke',
                    itemStyle: {
                        normal: {
                            color: 'rgba(80,80,80,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_smoke as $key => $value) {
                              echo round((($count_wp_smoke_ - $value->countt) / $count_wp_smoke_)*100);
                              #echo $count_wp_smoke_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - exsmoke',
                    type: 'bar',
                    stack: 'exsmoke',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_exsmoke as $key => $value) {
                              echo round(($value->countt / ($count_wp_exsmoke_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - exsmoke',
                    type: 'bar',
                    stack: 'exsmoke',
                    itemStyle: {
                        normal: {
                            color: 'rgba(110,110,110,0.7)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_exsmoke as $key => $value) {
                              echo round((($count_wp_exsmoke_ - $value->countt) / $count_wp_exsmoke_)*100);
                              #echo $count_wp_exsmoke_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - no smoke',
                    type: 'bar',
                    stack: 'nosmoke',
                    itemStyle: {
                        normal: {
                            color: 'rgba(89,141,160,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_nosmoke as $key => $value) {
                              echo round(($value->countt / ($count_wp_nosmoke_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - no smoke',
                    type: 'bar',
                    stack: 'nosmoke',
                    itemStyle: {
                        normal: {
                            color: 'rgba(140,140,140,0.5)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_nosmoke as $key => $value) {
                              echo round((($count_wp_nosmoke_ - $value->countt) / $count_wp_nosmoke_)*100);
                              #echo $count_wp_nosmoke_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //6-4
        var myChart = echarts.init(document.getElementById('plot6_4'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params) {
                    /*
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n(' + (params[0].value / 12 * 100).toFixed(0) + '%)<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n(' + (params[1].value / 12 * 100).toFixed(0) + '%)<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n(' + (params[2].value / 28 * 100).toFixed(0) + '%)<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n(' + (params[3].value / 28 * 100).toFixed(0) + '%)'
                        */
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n%<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n%<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n%<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n%'
                }
            },
            legend: {
                data: ['Mut - alcohol consumption', 'Wild-type - alcohol consumption', 'Mut - no alcohol consumption', 'Wild-type - no alcohol consumption']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_alchol as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_alchol as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut - alcohol consumption',
                    type: 'bar',
                    stack: 'al',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_alchol as $key => $value) {
                              echo round(($value->countt / ($count_wp_alchol_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - alcohol consumption',
                    type: 'bar',
                    stack: 'al',
                    itemStyle: {
                        normal: {
                            color: 'rgba(110,110,110,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_alchol as $key => $value) {
                              echo round((($count_wp_alchol_ - $value->countt) / $count_wp_alchol_)*100);
                              #echo $count_wp_alchol_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - no alcohol consumption',
                    type: 'bar',
                    stack: 'nal',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_noalchol as $key => $value) {
                              echo round(($value->countt / ($count_wp_noalchol_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - no alcohol consumption',
                    type: 'bar',
                    stack: 'nal',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_noalchol as $key => $value) {
                              echo round((($count_wp_noalchol_ - $value->countt) / $count_wp_noalchol_)*100);
                              #echo $count_wp_noalchol_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                }
            ]
        };

        // Load data into the ECharts instance
        myChart.setOption(option);

        //6-5
        var myChart = echarts.init(document.getElementById('plot6_5'));

        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function(params) {
                    /*
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n(' + (params[0].value / 22 * 100).toFixed(0) + '%)<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n(' + (params[1].value / 22 * 100).toFixed(0) + '%)<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n(' + (params[2].value / 19 * 100).toFixed(0) + '%)<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n(' + (params[3].value / 19 * 100).toFixed(0) + '%)'
                        */
                    return params[0].name + '<br/>' +
                        params[0].seriesName + ' : ' + params[0].value + '\n%<br/>' +
                        params[1].seriesName + ' : ' + params[1].value + '\n%<br/>' +
                        params[2].seriesName + ' : ' + params[2].value + '\n%<br/>' +
                        params[3].seriesName + ' : ' + params[3].value + '\n%'
                }
                /*,
                                formatter: function(params, ticket, callback) {
                                    let obj = params.map((item, index) => {
                                        if (item.value == undefined || item.value !== item.value) {
                                            item.value = 0;
                                        }
                                        let percent = (item.value / 22 * 100).toFixed(0)
                                        // 小圓點顯示
                                        let dotColor = '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:9px;height:9px;background-color:' + item.color + '"></span>'
                                        return dotColor + item.seriesName + ":" + item.value + '(' + percent + '%' + ')' + '</br>'
                                    }) return obj.join(''); // 去除','
                                }*/
            },
            legend: {
                data: ['Mut - red face', 'Wild-type - red face', 'Mut - no red face', 'Wild-type - no red face']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid',
                            width: 1
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_redface as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ],
                    axisLabel: {
                        fontStyle: "italic"
                    }
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($count_mut_redface as $key => $value) {
                              echo "'";
                              echo $value->gene;
                              echo "'";
                              echo ",";
                            }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'Mut - red face',
                    type: 'bar',
                    stack: 'red',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.9)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_redface as $key => $value) {
                              echo round(($value->countt / ($count_wp_redface_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - red face',
                    type: 'bar',
                    stack: 'red',
                    itemStyle: {
                        normal: {
                            color: 'rgba(110,110,110,0.85)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_redface as $key => $value) {
                              echo round((($count_wp_redface_ - $value->countt) / $count_wp_redface_)*100);
                              #echo $count_wp_redface_ - $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Mut - no red face',
                    type: 'bar',
                    stack: 'nored',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_noredface as $key => $value) {
                              echo round(($value->countt / ($count_wp_noredface_))*100);
                              #echo $value->countt;
                              echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Wild-type - no red face',
                    type: 'bar',
                    stack: 'nored',
                    itemStyle: {
                        normal: {
                            color: 'rgba(142,142,142,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($count_mut_noredface as $key => $value) {
                              echo round((($count_wp_noredface_ - $value->countt) / $count_wp_noredface_)*100);
                              #echo $count_wp_noredface_ - $value->countt;
                              echo ",";
                            }?>
                    ]

                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);


        var k = 1;
        var current_pageq = 1;
        var block_pagea = [0,
            <?php echo $a1_page.",".$a2_page.",".$a3_page.",".$a4_page.",".$a5_page;?>
        ];
        $(document).ready(function() {
            $(".container_program_6").hide();
            $("#rfm_plot6" + k).removeClass("rf-inactive6").addClass("rf-active6");
            $(".rf_block" + k + "_6").fadeIn(10, "swing");
        });

        $(".rf-item6").click(function() {
            current_pageq = 0;
            $("#rfm_plot6" + k).removeClass("rf-active6").addClass("rf-inactive6");
            $(".container_program_6").fadeOut(10, "swing");

            k = $(this).data('num');
            $("#rfm_plot6" + k).removeClass("rf-inactive6").addClass("rf-active6");

            $('.' + $(this).data('rel')).fadeIn(10, "swing");
        });
    </script>
    <script type="text/javascript">
        //7-1
        var myChart = echarts.init(document.getElementById('plot7_1'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['rate male', 'rate female', 'PR male', 'PR female', 'QRS male', 'QRS female', 'QT male', 'QT female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} ms'
                }
            }],
            series: [{
                    name: 'rate male',
                    type: 'bar',
                    /*stack: 'rate',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_male as $key => $value) {
                                  echo round($value->avgrate,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'rate female',
                    type: 'bar',
                    /*stack: 'rate',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_female as $key => $value) {
                          echo round($value->avgrate,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'PR male',
                    type: 'bar',
                    /*stack: 'PR',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(86, 100, 87,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_male as $key => $value) {
                                  echo round($value->avgPR,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'PR female',
                    type: 'bar',
                    /*stack: 'PR',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(138, 161, 139,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_female as $key => $value) {
                          echo round($value->avgPR,1);

                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'QRS male',
                    type: 'bar',
                    /*stack: 'QRS',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(36,107,99,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_male as $key => $value) {
                                  echo round($value->avgQRS,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'QRS female',
                    type: 'bar',
                    /*stack: 'QRS',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(60,127,127,0.5)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_female as $key => $value) {
                          echo round($value->avgQRS,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'QT male',
                    type: 'bar',
                    /*stack: 'QT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(80,80,80,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_male as $key => $value) {
                          echo round($value->avgQT,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'QT female',
                    type: 'bar',
                    /*stack: 'QT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(140,140,140,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($ekg_female as $key => $value) {
                          echo round($value->avgQT,1);
                          echo ",";
                        }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);
    </script>

    <script type="text/javascript">
        //8-1
        var myChart = echarts.init(document.getElementById('plot8_1'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['LA diameter male', 'LA diameter female', 'LVEDD male', 'LVEDD female', 'LVESD male', 'LVESD female', 'Ant LV wall thinkness male', 'Ant LV wall thinkness female', 'Post LV thinkness male', 'Post LV thinkness female']
            },
            grid: {
                top: 90
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} mm'
                }
            }],
            series: [{
                    name: 'LA diameter male',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgLA,1);
                                    echo ",";
                                    }?>
                    ]
                },
                { /*rgba(246,170,8,0.75)*/
                    name: 'LA diameter female',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgLA,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'LVEDD male',
                    type: 'bar',
                    /*stack: 'LVFE',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(86, 100, 87,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgLVE,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'LVEDD female',
                    type: 'bar',
                    /*stack: 'LVFE',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(138, 161, 139,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgLVE,1);

                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'LVESD male',
                    type: 'bar',
                    /*stack: 'LVES',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(197,200,91,0.8)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgLVES,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'LVESD female',
                    type: 'bar',
                    /*stack: 'LVES',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(211,221,152,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgLVES,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'Ant LV wall thinkness male',
                    type: 'bar',
                    /*stack: 'ALWT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(36,107,99,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                          echo round($value->avgALWT,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'Ant LV wall thinkness female',
                    type: 'bar',
                    /*stack: 'ALWT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(60,127,127,0.5)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgALWT,1);
                          echo ",";
                            }?>
                    ]
                },
                {
                    name: 'Post LV thinkness male',
                    type: 'bar',
                    /*stack: 'PLT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(80,80,80,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                          echo round($value->avgPLT,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'Post LV thinkness female',
                    type: 'bar',
                    /*stack: 'PLT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(140,140,140,0.6)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgPLT,1);
                          echo ",";
                            }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        //8-2
        var myChart = echarts.init(document.getElementById('plot8_2'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['RVSP male', 'RVSP female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} mmHg'
                }
            }],
            series: [{
                    name: 'RVSP male',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgRVSP,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'RVSP female',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgRVSP,1);
                          echo ",";
                        }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        //8-3
        var myChart = echarts.init(document.getElementById('plot8_3'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['LVEF male', 'LVEF female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} %'
                }
            }],
            series: [{
                    name: 'LVEF male',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgLVEF,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'LVEF female',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgLVEF,1);
                          echo ",";
                        }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        //8-4
        var myChart = echarts.init(document.getElementById('plot8_4'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['DT deceleration time male', 'DT deceleration time female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} msec'
                }
            }],
            series: [{
                    name: 'DT deceleration time male',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgDTDT,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'DT deceleration time female',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgDTDT,1);
                          echo ",";
                        }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        //8-5
        var myChart = echarts.init(document.getElementById('plot8_5'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['E wave male', 'E wave female', 'A wave male', 'A wave female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                },
                axisLabel: {
                    formatter: '{value} cm/s'
                }
            }],
            series: [{
                    name: 'E wave male',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgEW,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'E wave female',
                    type: 'bar',
                    /*stack: 'LA',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgEW,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'A wave male',
                    type: 'bar',
                    /*stack: 'ALWT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(86, 100, 87,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                          echo round($value->avgAW,1);
                          echo ",";
                        }?>
                    ]
                },
                {
                    name: 'A wave female',
                    type: 'bar',
                    /*stack: 'ALWT',*/
                    itemStyle: {
                        normal: {
                            color: 'rgba(138, 161, 139,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgAW,1);
                          echo ",";
                            }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        //8-6
        var myChart = echarts.init(document.getElementById('plot8_6'));


        var option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: { // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            legend: {
                data: ['EA division male', 'EA division female']
            },
            toolbox: {
                show: false
            },
            calculable: true,
            xAxis: [{
                    type: 'category',
                    axisLine: { // 轴线
                        show: true,
                        lineStyle: {
                            color: 'gray',
                            type: 'solid'
                        }
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                },
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    splitArea: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    },
                    data: [
                        <?php foreach ($ekg_all as $key => $value) {
                                echo "'";
                                echo $value->Disease;
                                echo "'";
                                echo ",";
                              }?>
                    ]
                }
            ],
            yAxis: [{
                type: 'value',
                axisLine: { // 轴线
                    show: true,
                    lineStyle: {
                        color: 'gray',
                        type: 'solid',
                        width: 1
                    }
                }
            }],
            series: [{
                    name: 'EA division male',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: 'rgba(18,30,54,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_male as $key => $value) {
                                  echo round($value->avgEAD,1);
                                    echo ",";
                                    }?>
                    ]
                },
                {
                    name: 'EA division female',
                    type: 'bar',
                    itemStyle: {
                        normal: {
                            color: 'rgba(35,87,107,0.75)',
                            label: {
                                show: false
                            }
                        }
                    },
                    data: [
                        <?php foreach ($Echo_female as $key => $value) {
                          echo round($value->avgEAD,1);
                          echo ",";
                        }?>
                    ]
                }
            ]
        };
        // Load data into the ECharts instance
        myChart.setOption(option);

        var s = 1;
        var current_pageq = 1;
        var block_pagea = [0,
            <?php echo $s1_page.",".$s2_page.",".$s3_page.",".$s4_page.",".$s5_page.",".$s6_page;?>
        ];
        $(document).ready(function() {
            $(".container_program_8").hide();
            $("#rfm_plot8" + s).removeClass("rf-inactive8").addClass("rf-active8");
            $(".rf_block" + s + "_8").fadeIn(10, "swing");
        });

        $(".rf-item8").click(function() {
            $("#rfm_plot8" + s).removeClass("rf-active8").addClass("rf-inactive8");
            $(".container_program_8").fadeOut(10, "swing");

            s = $(this).data('num');
            $("#rfm_plot8" + s).removeClass("rf-inactive8").addClass("rf-active8");

            $('.' + $(this).data('rel')).fadeIn(10, "swing");
        });
    </script>

    <!-- plot 9 -->
    <!-- Load d3.js -->
    <script src="https://d3js.org/d3.v4.js"></script>

    <!-- Function for radial charts -->
    <script src="https://cdn.jsdelivr.net/gh/holtzy/D3-graph-gallery@master/LIB/d3-scale-radial.js"></script>

    <script type="text/javascript">
        // set the dimensions and margins of the graph
        var margin = {
                top: 100,
                right: 0,
                bottom: 100,
                left: 0
            },
            width = 860 - margin.left - margin.right,
            height = 860 - margin.top - margin.bottom,
            innerRadius = 90,
            outerRadius = Math.min(width, height) / 2; // the outerRadius goes from the middle of the SVG area to the border

        // append the svg object
        var svg = d3.select("#plot9")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + (width / 2 + margin.left) + "," + (height / 2 + margin.top) + ")");

        var data = [{
                number: <?php echo  $count_mut_1_; ?>,
                variants: "RYR2|g.chr1:237765380A>G|c.4652A>G|p.N1551S"
            },
            {
                number: <?php echo  $count_mut_2_; ?>,
                variants: "MYBPC3|g.chr11:47353740G>A|c.3697C>T|p.Q1233X"
            },
            {
                number: <?php echo  $count_mut_4_; ?>,
                variants: "DSP|g.chr6:7569480A>T|c.1481A>T|p.Y494F"
            },
            {
                number: <?php echo  $count_mut_6_; ?>,
                variants: "SCN5A|g.chr3:38608067C>T|c.3673G>A|p.E1225K"
            },
            {
                number: <?php echo  $count_mut_8_; ?>,
                variants: "DSP|g.chr6:7583238C>A|c.5743C>A|p.R1915S"
            },
            {
                number: <?php echo  $count_mut_10_; ?>,
                variants: "DSP|g.chr6:7580187G>C|c.3764G>C|p.R1255T"
            },
            {
                number: <?php echo  $count_mut_11_; ?>,
                variants: "DSP|g.chr6:7574952A>G|c.2360A>G|p.Y787C"
            },
            {
                number: <?php echo  $count_mut_12_; ?>,
                variants: "SCN5A|g.chr3:38592513C>T|c.5350G>A|p.E1784K"
            },
            {
                number: <?php echo  $count_mut_15_; ?>,
                variants: "RYR2|g.chr1:237777474A>C|c.5046A>C|p.E1682D"
            },
            {
                number: <?php echo  $count_mut_16_; ?>,
                variants: "CACNB2|g.chr10:18828261C>T|c.1591C>T|p.R531C"
            },

            {
                number: <?php echo  $count_mut_17_; ?>,
                variants: "RYR2|g.chr1:237778084G>A|c.5656G>A|p.G1886S"
            },
            {
                number: <?php echo  $count_mut_18_; ?>,
                variants: "DSP|g.chr6:7580346G>A|c.3923G>A|p.R1308Q"
            },
            {
                number: <?php echo  $count_mut_23_; ?>,
                variants: "PKP2|g.chr12:32974441G>C|c.1862C>G|p.P621R"
            },
            {
                number: <?php echo  $count_mut_24_; ?>,
                variants: "DSP|g.chr6:7581636G>A|c.5213G>A|p.R1738Q"
            },
            {
                number: <?php echo  $count_mut_25_; ?>,
                variants: "SCN5A|g.chr3:38616915G>A|c.3539C>T|p.A1180V"
            },

            {
                number: <?php echo  $count_mut_26_; ?>,
                variants: "SCN5A|g.chr3:38604001T>C|c.3706A>G|p.T1236A"
            },
            {
                number: <?php echo  $count_mut_28_; ?>,
                variants: "MYBPC3|g.chr11:47370063A>C|c.684T>G|p.D228E"
            },
            {
                number: <?php echo  $count_mut_29_; ?>,
                variants: "SCN5A|g.chr3:38622757G>A|c.2893C>T|p.R965C"
            },
            {
                number: <?php echo  $count_mut_30_; ?>,
                variants: "SCN5A|g.chr3:38655249T>C|c.688A>G|p.I230V"
            },
            {
                number: <?php echo  $count_mut_33_; ?>,
                variants: "MYH7|g.chr14:23883054C>G|c.5704G>C|p.E1902Q"
            },

            {
                number: <?php echo  $count_mut_37_; ?>,
                variants: "KCNH2|g.chr7:150647022G>A|c.2632C>T|p.R878C"
            },
            {
                number: <?php echo  $count_mut_38_; ?>,
                variants: "RYR2|g.chr1:237754226C>T|c.4094C>T|p.A1365V"
            },
            {
                number: <?php echo  $count_mut_39_; ?>,
                variants: "MYBPC3|g.chr11:47364231G>A|c.1522C>T|p.Q508X"
            },
            {
                number: <?php echo  $count_mut_40_; ?>,
                variants: "CACNA1C|g.chr12:2800220A>G|c.6377A>G|p.N2126S"
            },
            {
                number: <?php echo  $count_mut_41_; ?>,
                variants: "KCNQ1|g.chr11:2591955G>A|c.575G>A|p.R192H"
            },

            {
                number: <?php echo  $count_mut_42_; ?>,
                variants: "SCN5A|g.chr3:38598739C>A|c.4282G>T|p.A1428S"
            },
            {
                number: <?php echo  $count_mut_43_; ?>,
                variants: "DSP|g.chr6:7584294A>T|c.6799A>T|p.T2267S"
            },
            {
                number: <?php echo  $count_mut_44_; ?>,
                variants: "SCN5A|g.chr3:38640439C>A|c.1993G>T|p.A665S"
            },
            {
                number: <?php echo  $count_mut_48_; ?>,
                variants: "DSP|g.chr6:7568727T>G|c.1324T>G|p.S442A"
            },
            {
                number: <?php echo  $count_mut_49_; ?>,
                variants: "SCN5A|g.chr3:38616912A>G|c.3380T>C|p.V1127A"
            },

            {
                number: <?php echo  $count_mut_51_; ?>,
                variants: "SCN5A|g.chr3:38620946G>A|c.3266C>T|p.P1089L"
            },
            {
                number: <?php echo  $count_mut_52_; ?>,
                variants: "CACNB2|g.chr10:18828169G>A|c.1499G>A|p.G500D"
            },
            {
                number: <?php echo  $count_mut_53_; ?>,
                variants: "DSP|g.chr6:7559521G>T|c.485G>T|p.R162L"
            },
            {
                number: <?php echo  $count_mut_55_; ?>,
                variants: "CACNB2|g.chr10:18828605A>C|c.1935A>C|p.K645N"
            },
            {
                number: <?php echo  $count_mut_56_; ?>,
                variants: "RYR2|g.chr1:237778042G>A|c.5614G>A|p.D1872N"
            },

            {
                number: <?php echo  $count_mut_58_; ?>,
                variants: "DSP|g.chr6:7556049A>G|c.269A>G|p.Q90R"
            },
            {
                number: <?php echo  $count_mut_59_; ?>,
                variants: "PKP2|g.chr12:33021934A>G|c.1097T>C|p.L366P"
            },
            {
                number: <?php echo  $count_mut_60_; ?>,
                variants: "KCNH2|g.chr7:150644856G>A|c.2803C>T|p.P935S"
            },
            {
                number: <?php echo  $count_mut_62_; ?>,
                variants: "DSP|g.chr6:7584484T>G|c.6989T>G|p.L2330R"
            },
            {
                number: <?php echo  $count_mut_63_; ?>,
                variants: "DSP|g.chr6:7585411G>A|c.7916G>A|p.R2639Q"
            },

            {
                number: <?php echo  $count_mut_64_; ?>,
                variants: "KCNE1|g.chr21:35821680C>T|c.253G>A|p.D85N"
            },
            {
                number: <?php echo  $count_mut_66_; ?>,
                variants: "MYBPC3|g.chr11:47371336G>A|c.643C>T|p.R215C"
            },
            {
                number: <?php echo  $count_mut_70_; ?>,
                variants: "MYBPC3|g.chr11:47367848C>T|c.1000G>A|p.E334K"
            },
            {
                number: <?php echo  $count_mut_72_; ?>,
                variants: "RYR2|g.chr1:237666602C>T|c.2410C>T|p.L804F"
            },
/*            {
                number: <?php echo  $count_mut_77_; ?>,
                variants: "SCN5A|g.chr3:38601865C>T|c.4018G>A|p.V1340I"
            },

            {
                number: <?php echo  $count_mut_79_; ?>,
                variants: "MYBPC3|g.chr11:47356737G>C|c.2761C>G|p.Q921E"
            },
            {
                number: <?php echo  $count_mut_80_; ?>,
                variants: "MYBPC3|g.chr11:47372978C>T|c.104G>A|p.R35Q"
            },
            {
                number: <?php echo  $count_mut_81_; ?>,
                variants: "DSP|g.chr6:7580913G>A|c.4490G>A|p.R1497Q"
            },
*/            {
                number: <?php echo  $count_mut_82_; ?>,
                variants: "MYBPC3|g.chr11:47364234C>T|c.1519G>A|p.G507R"
            },
/*            {
                number: <?php echo  $count_mut_84_; ?>,
                variants: "DSP|g.chr6:7583998G>C|c.6503G>C|p.S2168T"
            },
*/            {
                number: <?php echo  $count_mut_85_; ?>,
                variants: "RYR2|g.chr1:237948232G>A|c.13220G>A|p.S4407N"
            },
/*            {
                number: <?php echo  $count_mut_88_; ?>,
                variants: "PKP2|g.chr12:32974457G>A|c.1846C>T|p.Q616X"
            },
            {
                number: <?php echo  $count_mut_89_; ?>,
                variants: "CACNA1C|g.chr12:2788862G>A|c.5344G>A|p.A1782T"
            },
*/            {
                number: <?php echo  $count_mut_90_; ?>,
                variants: "DSP|g.chr6:7585539C>G|c.8044C>G|p.Q2682E"
            },
            {
                number: <?php echo  $count_mut_92_; ?>,
                variants: "MYH7|g.chr14:23902865G>A|c.77C>T|p.A26V"
            },
/*            {
                number: <?php echo  $count_mut_93_; ?>,
                variants: "MYH7|g.chr14:23892874T>C|c.2981A>G|p.K994R"
            },
*/            {
                number: <?php echo  $count_mut_94_; ?>,
                variants: "SCN5A|g.chr3:38655260G>A|c.677C>T|p.A226V"
            },
/*            {
                number: <?php echo  $count_mut_101_; ?>,
                variants: "MYBPC3|g.chr11:47355161G>A|c.3137C>T|p.T1046M"
            },
            {
                number: <?php echo  $count_mut_102_; ?>,
                variants: "RYR2|g.chr1:237664082T>A|c.2275T>A|p.L759I"
            },
*/            {
                number: <?php echo  $count_mut_103_; ?>,
                variants: "SCN5A|g.chr3:38645534A>C|c.1559T>G|p.M520R"
            },
            {
                number: <?php echo  $count_mut_105_; ?>,
                variants: "SCN5A|g.chr3:38640439C>T|c.1993G>A|p.A665T"
            },
            {
                number: <?php echo  $count_mut_107_; ?>,
                variants: "MYBPC3|g.chr11:47355475G>C|c.2992C>G|p.Q998E"
            },
            {
                number: <?php echo  $count_mut_111_; ?>,
                variants: "CACNA1C|g.chr12:2788847C>T|c.5329C>T|p.R1777C"
            },
/*            {
                number: <?php echo  $count_mut_112_; ?>,
                variants: "CACNB2|g.chr10:18795465C>A|c.659C>A|p.P220H"
            }*/
        ];



        // Scales
        var x = d3.scaleBand()
            .range([0, 2 * Math.PI]) // X axis goes from 0 to 2pi = all around the circle. If I stop at 1Pi, it will be around a half circle
            .align(0) // This does nothing
            .domain(data.map(function(d) {
                return d.variants;
            })); // The domain of the X axis is the list of states.
        var y = d3.scaleRadial()
            .range([innerRadius, outerRadius]) // Domain will be define later.
            .domain([0, 20]); // Domain of Y is from 0 to the max seen in the data

        // Add the bars
        svg.append("g")
            .selectAll("path")
            .data(data)
            .enter()
            .append("path")
            .attr("fill", "#ABC3BF")
            .attr("d", d3.arc() // imagine your doing a part of a donut plot
                .innerRadius(innerRadius)
                .outerRadius(function(d) {
                    return y(d['number']);
                })
                .startAngle(function(d) {
                    return x(d.variants);
                })
                .endAngle(function(d) {
                    return x(d.variants) + x.bandwidth();
                })
                .padAngle(0.01)
                .padRadius(innerRadius))

        // Add the labels
        svg.append("g")
            .selectAll("g")
            .data(data)
            .enter()
            .append("g")
            .attr("text-anchor", function(d) {
                return (x(d.variants) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "end" : "start";
            })
            .attr("transform", function(d) {
                return "rotate(" + ((x(d.variants) + x.bandwidth() / 2) * 180 / Math.PI - 90) + ")" + "translate(" + (y(d['number']) + 10) + ",0)";
            })
            .append("text")
            .text(function(d) {
                return (d.variants)
            })
            .attr("transform", function(d) {
                return (x(d.variants) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "rotate(180)" : "rotate(0)";
            })
            .style("font-size", "11px")
            .attr("alignment-baseline", "middle")

        svg.append("g")
            .selectAll("g")
            .data(data)
            .enter()
            .append("g")
            .attr("text-anchor", function(d) {
                return (x(d.variants) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "end" : "start";
            })
            .attr("transform", function(d) {
                return "rotate(" + ((x(d.variants) + x.bandwidth() / 2) * 180 / Math.PI - 90) + ")" + "translate(" + (y(d['number']) - 10) + ",0)";
            })
            .append("text")
            .text(function(d) {
                return (d.number)
            })
            .attr("transform", function(d) {
                return (x(d.variants) + x.bandwidth() / 2 + Math.PI) % (2 * Math.PI) < Math.PI ? "rotate(180)" : "rotate(0)";
            })
            .style("font-size", "11px")
            .attr("alignment-baseline", "middle")
    </script>

    @endsection