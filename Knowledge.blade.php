@extends('frontend.layouts.master')
@section('title', 'Knowledge')
@section('content')
@section('nav_Knowledge', 'active')

<link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet">

<div class="container-in">
    <div class="container-fluid">
        <div class="contain_other_page" style="padding-bottom:5%;">
            <div class="container_small" style="font-weight:600;">
                <p>HRS/EHRA Expert Consensus Statement on the State of Genetic Testing for the Channelopathies and Cardiomyopathies: This document was developed as a partnership
                    between the Heart Rhythm Society (HRS) and the European Heart Rhythm Association (EHRA) </p>
                <hr id="container_hr">
                <a href="https://academic.oup.com/europace/article/13/8/1077/516373"><button type="button" class="btn btn-primary" style="background-color:#566457; border:0; ">Full txt</button></a>

            </div>
            <div class="container_small" style="font-weight:600;">
                <p>ACMG recommendations for reporting of incidental findings in clinical exome and genome sequencing</p>
                <hr id="container_hr">
                <a href="https://www.ncbi.nlm.nih.gov/pubmed/23788249"><button type="button" class="btn btn-primary" style="background-color:#566457; border:0; ">Full txt</button></a>

            </div>
            <div class="container_small" style="font-weight:600;">
                <p>Standards and guidelines for the interpretation of sequence variants: a joint consensus recommendation of the American College of Medical Genetics and Genomics
                    and the Association for Molecular Pathology.</p>
                <hr id="container_hr">

                <a href="https://www.ncbi.nlm.nih.gov/pubmed/25741868"><button type="button" class="btn btn-primary" style="background-color:#566457; border:0; ">Full txt</button></a>


            </div>

        </div>
    </div>
</div>
@endsection