<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav" style="padding-top:0!important;padding-bottom:0!important;">
    <div class="nav_container" style="background-color:#121E36; padding:0;">
        <!--<img src="svg/ICCSCDlogo.svg" style="height:auto; width:300px;align:"left";"></img>
    -->
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Menu</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav mx-auto">
                <li class="nav-item @yield('nav_home') mx-lg-4">
                    <a class="nav-link text-expanded" href="{{route('home')}}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item @yield('nav_Search') mx-lg-4">
                    <a class="nav-link  text-expanded" href="{{route('Search')}}">Search</a>
                </li>
                <li class="nav-item @yield('nav_Browse') mx-lg-4">
                    <a class="nav-link text-expanded" href="{{route('Browse')}}">Browse</a>
                </li>
                <li class="nav-item @yield('nav_Statistics') mx-lg-4">
                    <a class="nav-link text-expanded" href="{{route('Statistics')}}">Statistics</a>
                </li>
                <li class="nav-item @yield('nav_Knowledge') mx-lg-4">
                    <a class="nav-link text-expanded" href="{{route('Knowledge')}}">Knowledge</a>
                </li>
                <li class="nav-item @yield('nav_Contactus') mx-lg-4">
                    <a class="nav-link text-expanded" href="{{route('Contactus')}}">Contact us</a>
                </li>
            </ul>
        </div>
    </div>
</nav>