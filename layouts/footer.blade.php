<!-- footer -->
<footer class="footer text-faded text-center py-5">
    <div class="container">
        <p class="footer_word">Copyright &copy; CHSLab, Department of Biological Science and Technology, Institute of Bioinformatics and Systems Biology, National Chiao Tung University, Hsinchu, Taiwan
        </p>
    </div>
</footer>
<!-- footer end -->