<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" >
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css">
    <link rel="stylesheet" type="text/css" href="css/text.css">

    <title></title>
  </head>
  <body>
    <header>
        <h1 class="logo">myblog</h1>
      <nav>
        <a href="#">所有文章</a>
        <a href="#">關於我們</a>
        <a href="#">聯絡我們</a>
      </nav>
    </header>

    <div class="containerinat">
      <div class="banner-at"></div>
      <div class="articlee">
        <h3>Do what defines you</h3>
        <p>我們跟每一個滿懷理想，想讓現況更好的你一樣熱愛生活，對未知領域永遠充滿探索熱情。因為關心公共事務，所以言談有點負能量，但行動依舊積極正面。心中都有一個最棒的群眾募資提案，才讓我們在全台灣最大的群眾募資平台相遇，一起致力於讓更多價值、理念得以被看見，讓好的點子走入生活。我們投身於社會的變革之中，不自量力地想要讓這改變的巨輪動得快一些。在 flyingV，我們一起努力工作，一起大聲歡笑，激盪新奇的點子讓眾人的理想成真。</p>
        <p>我們跟每一個滿懷理想，想讓現況更好的你一樣熱愛生活，對未知領域永遠充滿探索熱情。因為關心公共事務，所以言談有點負能量，但行動依舊積極正面。心中都有一個最棒的群眾募資提案，才讓我們在全台灣最大的群眾募資平台相遇，一起致力於讓更多價值、理念得以被看見，讓好的點子走入生活。我們投身於社會的變革之中，不自量力地想要讓這改變的巨輪動得快一些。在 flyingV，我們一起努力工作，一起大聲歡笑，激盪新奇的點子讓眾人的理想成真。</p>
      </div>
    </div>
    <div class="containerinatright">
      <!-- article list  -->
      <div class="article-list-at">
        <h4>相關文章</h4>
        <div class="article-item-at">
          <a href="#">
          <h5>little kid</h5>
          <p>I naively assumed that there would be an opacity property on the background image, but it turns out that's not so.#</p>
          </a>
        </div>
        <div class="article-item-at">
          <h5>little kid</h5>
          <p>a little kid</p>
        </div>
        <div class="article-item-at">
          <h5>little kid</h5>
          <p>a little kid</p>
        </div>
        <div class="article-item-at">
          <h5>little kid</h5>
          <p>a little kid</p>
        </div>
        <div class="article-item-at">
          <h5>little kid</h5>
          <p>a little kid</p>
        </div>
        <div class="article-item-at">
          <h5>little kid</h5>
          <p>a little kid</p>
        </div>
      </div>
    </div>
    <footer><p>all Copyright reserves to shepherd media wordshop</p></footer>
  </body>
</html>
