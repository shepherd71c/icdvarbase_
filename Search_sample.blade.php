@extends('frontend.layouts.master')
@section('title', 'Search')
@section('content')
@section('nav_Search', 'active')

<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">
                <div class="rf-form_list_block">


                    <div class="container_program rf_block1_0">
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th title="Field #1" style="width:20%;background-color:#CAB44F ;">Gene</th>
                                    <th title="Field #2" style="width:20%;background-color:#CAB44F ;">Variants</th>
                                    <th title="Field #3" style="width:20%;background-color:#CAB44F ;">ACMG Classify</th>
                                    <th title="Field #4" style="width:20%;background-color:#CAB44F ;">Case Number</th>
                                    <th title="Field #5" style="width:20%;background-color:#CAB44F ;">Disease of Cardiology</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($advance_search_bysample as $key => $data)
                                <tr>

                                    <th style="font-style:italic;">{{$data->gene}}</th>
                                    <th>
                                        <form action="{{url('variants')}}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="name" value={{$data->variants}}>
                                            <button type="submit" name="button">{{$data->variants}}</button>
                                        </form>
                                    </th>
                                    <th>{{$data->ACMG_classify}}</th>
                                    <th>
                                    <?php $c = explode(",",$data->alley_name);
                                        ?>
                                    @foreach ($c as $key => $datadata)
                                    <?php
                                        $c1 = explode("-",$datadata);
                                        $c2 = $c1[1]; #NTUH start = SUD-103

                                        if($c2<103) {
                                        ?>
                                    <form action="{{url('Browse_case')}}" method="post">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="name" value={{$datadata}}>
                                        <button type="submit" name="button">{{$datadata}}</button>
                                    </form>
                                    <?php
                                    }
                                    else{
                                    ?>
                                    {{$datadata}}<br>
                                    <?php
                                    }
                                    ?>
                                    @endforeach
                                    </th>
                                    <th>{{$data->Disease}}</th>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>


                    </div>

                </div>
            </div>
        </div>
</section>

@endsection