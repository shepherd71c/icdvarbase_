@extends('frontend.layouts.master')
@section('title', 'Browse')
@section('content')

<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">


                <div class="rf-form_list_block">
                    <ul style="margin-bottom:0;    border-bottom: #566457 4px solid;">
                        <li>
                            <div class="rf-item" data-num="1" data-rel="rf_block1_0" style="cursor: auto; ">
                                <div id="rfm1" class="rf-active">{{$case_name}}</div>
                            </div>
                        </li>
                    </ul>
                    <div class="container_program rf_block1_0 rf-form_list_block_right">
                        <div class="container_it">
                            <table>
                                <tr>
                                    <td>
                                        @foreach($case_clinical as $key => $data)
                                        <?php
                                          if($data->Gende == "Female"){

                                        ?>
                                        <div class="profile_img"><img src="img/female_icon.png" alt=""></div>
                                        <?php
                                        }
                                          else{
                                        ?>
                                        <div class="profile_img"><img src="img/male-icon-7920.png" alt=""></div>
                                        <?php
                                      }
                                        ?>
                                        @endforeach

                                    </td>
                                    <td>

                                        <ul>

                                            <li>
                                                <div class="rf-item_right" data-num="1" data-rel="rf_block_right1_0" style="cursor: pointer; ">
                                                    <div id="rfm_right1" class="rf-inactive_right">ICC/SCD report</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="2" data-rel="rf_block_right2_0" style="cursor: pointer; ">
                                                    <div id="rfm_right2" class="rf-inactive_right">Clinical history</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="3" data-rel="rf_block_right3_0" style="cursor: pointer; ">
                                                    <div id="rfm_right3" class="rf-inactive_right">Medication</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="4" data-rel="rf_block_right4_0" style="cursor: pointer; ">
                                                    <div id="rfm_right4" class="rf-inactive_right">Family history</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="5" data-rel="rf_block_right5_0" style="cursor: pointer; ">
                                                    <div id="rfm_right5" class="rf-inactive_right">Biochemistry</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="6" data-rel="rf_block_right6_0" style="cursor: pointer; ">
                                                    <div id="rfm_right6" class="rf-inactive_right">EKG</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="7" data-rel="rf_block_right7_0" style="cursor: pointer; ">
                                                    <div id="rfm_right7" class="rf-inactive_right">Echo</div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class="rf-item_right" data-num="8" data-rel="rf_block_right8_0" style="cursor: pointer; ">
                                                    <div id="rfm_right8" class="rf-inactive_right">24Hr_holter</div>
                                                </div>
                                            </li>

                                            <li>
                                                <div class=" rf-item_right" data-num="9" data-rel="rf_block_right9_0" style="cursor: pointer; ">
                                                    <div id="rfm_right9" class="rf-inactive_right">Thallium scan</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="10" data-rel="rf_block_right10_0" style="cursor: pointer; ">
                                                    <div id="rfm_right10" class="rf-inactive_right">Treadmill</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="11" data-rel="rf_block_right11_0" style="cursor: pointer; ">
                                                    <div id="rfm_right11" class="rf-inactive_right">MRI</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="12" data-rel="rf_block_right12_0" style="cursor: pointer; ">
                                                    <div id="rfm_right12" class="rf-inactive_right">SAECG</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="13" data-rel="rf_block_right13_0" style="cursor: pointer; ">
                                                    <div id="rfm_right13" class="rf-inactive_right">Coronary artery angiography</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="14" data-rel="rf_block_right14_0" style="cursor: pointer; ">
                                                    <div id="rfm_right14" class="rf-inactive_right">EPS study</div>
                                                </div>
                                            </li>
<!--
                                            <li>
                                                <div class="rf-item_right" data-num="15" data-rel="rf_block_right15_0" style="cursor: pointer; ">
                                                    <div id="rfm_right15" class="rf-inactive_right">Following_form</div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="rf-item_right" data-num="16" data-rel="rf_block_right16_0" style="cursor: pointer; ">
                                                    <div id="rfm_right16" class="rf-inactive_right">Specimen</div>
                                                </div>
                                            </li>
-->
										</ul>


                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- block 1 -->
                        <?php
                    $i=1;
                    $b1_page=0;
                    ?>
                        <div class="container_program_right rf_block_right1_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th title="Field #1" style="width:13%;background-color:#ABC3BF ;">Gene</th>
                                        <th title="Field #2" style="width:16%;background-color:#ABC3BF ;">Variantion</th>
                                        <th title="Field #3" style="width:16%;background-color:#ABC3BF ;">Variant Type</th>
                                        <th title="Field #4" style="width:13%;background-color:#ABC3BF ;">Zygosity</th>
                                        <th title="Field #5" style="width:16%;background-color:#ABC3BF ;">ACMG Classification</th>
                                        <th title="Field #6" style="width:13%;background-color:#ABC3BF ;">TaiwanBioBank Frequency</th>
                                    </tr>


                                </thead>
                                <tbody>
                                    @foreach($case_browser as $key => $data)

                                    <?php $Taiwanbiobank = explode(":",$data->TaiwanBioBank);
                                              $rate = $Taiwanbiobank[1];
                                              $zygo = $Taiwanbiobank[0];
                                          ?>

                                    <tr>
                                        <th style="font-style:italic;">{{$data->gene}}</th>
                                        <th>
                                            <form action="{{url('variants')}}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="name" value={{$data->variants}}>
                                                <button type="submit" name="button">{{$data->variants}}</button>
                                            </form>
                                        </th>
                                        <th>{{$data->mutation_type}}</th>
                                        <th>{{$zygo}}</th>
                                        <th>{{$data->ACMG_classify}}</th>
                                        <th>
                                            <?php
                                          if($rate != '-'){
                                          echo (float)$rate*100;
                                          echo '%';
                                          }
                                          else {
                                          echo $rate;
                                          }
                                        ?>
                                        </th>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 2 -->
                        <?php
                    $i=2;
                    $b2_page=0;
                    ?>
                        <div class="container_program_right rf_block_right2_0">

                            <!--section1-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 1 General Data</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical as $key => $data)
                                    <tr>
                                        <th>Sampling Date</th>
                                        <th>{{$data->SDate}}</th>
                                    </tr>
                                    <tr>
                                        <th>Sampling Time</th>
                                        <th>{{$data->ST}}</th>
                                    </tr>
                                    <tr>
                                        <th>kinship</th>
                                        <th>{{$data->kinship}}</th>
                                    </tr>
                                    <tr>
                                        <th>其他 與個案關係</th>
                                        <th>{{$data->Kinship_meno}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Age(years old)</th>
                                        <th>{{$data->Age}}</th>
                                    </tr>
                                    <tr>
                                        <th>Gender</th>
                                        <th>{{$data->Gende}}</th>
                                    </tr>
                                    <tr>
                                        <th>If female,is menopause or not</th>
                                        <th>{{$data->FMN}}</th>
                                    </tr>
                                    <tr>
                                        <th>Height</th>
                                        <th>{{$data->Height}}cm</th>
                                    </tr>
                                    <tr>
                                        <th>Weight</th>
                                        <th>{{$data->Weight}}kg</th>
                                    </tr>
                                    <tr>
                                        <th>WC(Waistline)(cm)</th>
                                        <th>{{$data->WC}}</th>
                                    </tr>
                                    <tr>
                                        <th>SBP(systolic blood pressure)(mmHg)</th>
                                        <th>{{$data->SBP}}</th>
                                    </tr>
                                    <tr>
                                        <th>DBP(Diastolic blood pressure) (mmHg)</th>
                                        <th>{{$data->DBP}}</th>
                                    </tr>
                                    <tr>
                                        <th>PR(pulse)(time/min)</th>
                                        <th>{{$data->PR}}</th>
                                    </tr>
                                    <tr>
                                        <th>Smoke</th>
                                        <th>{{$data->Smoke}}</th>
                                    </tr>
                                    <tr>
                                        <th>If smoker, how many per day(支/day)</th>
                                        <th>{{$data->AD}}</th>
                                    </tr>
                                    <tr>
                                        <th>If smoker/ex-smoker, for how many years(years)</th>
                                        <th>{{$data->DOS}}</th>
                                    </tr>
                                    <tr>
                                        <th>If ex-smoker, quit for how many years(years)</th>
                                        <th>{{$data->DOQ}}</th>
                                    </tr>
                                    <tr>
                                        <th>Alchol</th>
                                        <th>{{$data->Alc}}</th>
                                    </tr>
                                    <tr>
                                        <th>If drink, how much per day(cc/day)</th>
                                        <th>{{$data->hma}}</th>
                                    </tr>
                                    <tr>
                                        <th>If drink, whitch kind</th>
                                        <th>{{$data->kod}}</th>
                                    </tr>
                                    <tr>
                                        <th>Red face</th>
                                        <th>{{$data->Rfad}}</th>
                                    </tr>
                                    <tr>
                                        <th>Vegeterian</th>
                                        <th>{{$data->veg}}</th>
                                    </tr>

                                    @endforeach
                                </tbody>
                                <!--section2-->
                            </table>
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>


                                    <th colspan="2" title="Field #1" style="width:50%;background-color:#ABC3BF ;">Section 2 Diagnosis of Cardiology</th>

                                </thead>
                                <tbody>
                                    @foreach($case_clinical2 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Diagnosis of Cardiology</th>
                                        <th>{{$data->Disease}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <!--section3-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="width:50%;background-color:#ABC3BF ;">Section 3 Medical History</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical3 as $key => $data)
                                    <tr>
                                        <th style="width:50%;">Thromboembolism disease</th>
                                        <th>{{$data->TD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If stroke,stroke date</th>
                                        <th>{{$data->SD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If stroke,stroke type</th>
                                        <th>{{$data->ST}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If TIA, TIA date</th>
                                        <th>{{$data->TIAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If systemic embolism, systemic embolism Date</th>
                                        <th>{{$data->SED}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If systemic embolism, systemic embolism type</th>
                                        <th>{{$data->SET}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pulmonary embolism</th>
                                        <th>{{$data->PE}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If Pulmonary embolism, Pulmonary embolism date</th>
                                        <th>{{$data->PED}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">gastrointestinal bleeding</th>
                                        <th>{{$data->GB}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Hypertention</th>
                                        <th>{{$data->HTN}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Diabetes</th>
                                        <th>{{$data->DM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Heart failure</th>
                                        <th>{{$data->HF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Hyperlipidemia</th>
                                        <th>{{$data->HYP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AMI</th>
                                        <th>{{$data->AMI}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PAOD</th>
                                        <th>{{$data->PAOD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">CAD</th>
                                        <th>{{$data->CAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If CAD, CAD vessel LAD</th>
                                        <th>{{$data->CALAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If CAD, CAD vessel RCA</th>
                                        <th>{{$data->CARCA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">If CAD, CAD vessel LCX</th>
                                        <th>{{$data->CALCX}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Heart valve disease</th>
                                        <th>{{$data->HVD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">eart valve disease memo</th>
                                        <th>{{$data->HVDM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Other Arrhythmia</th>
                                        <th>{{$data->OAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Other Arrhythmia memo</th>
                                        <th>{{$data->OADM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Congenital heart disease</th>
                                        <th>{{$data->CHD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Congenital heart disease type</th>
                                        <th>{{$data->CHDT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pacemaker</th>
                                        <th>{{$data->PACE}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pacemaker Memo</th>
                                        <th>{{$data->PACEM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Thyroid function</th>
                                        <th>{{$data->TYF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">COPD</th>
                                        <th>{{$data->COPD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Liver disease</th>
                                        <th>{{$data->LD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Liver disease(If have)</th>
                                        <th>{{$data->LDD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Other Hapatitis(If have)</th>
                                        <th>{{$data->OHA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Chronic kidney disease(CKD)</th>
                                        <th>{{$data->CKD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Sleep apnea</th>
                                        <th>{{$data->SAP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Syncope</th>
                                        <th>{{$data->SYNC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">藥物濫用</th>
                                        <th>{{$data->Drug}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">精神疾病</th>
                                        <th>{{$data->ND}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">其他過去疾病史</th>
                                        <th>{{$data->ODisease}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 3 -->
                        <?php
                    $i=3;
                    $b3_page=0;
                    ?>
                        <div class="container_program_right rf_block_right3_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Cardiovascular Agents Intake</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical4 as $key => $data)
                                    <tr>
                                        <th style="width:50%;">antiplatelet</th>
                                        <th>{{$data->ant}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">pleetal</th>
                                        <th>{{$data->ple}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Dronedarone</th>
                                        <th>{{$data->Dro}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Amiodarone</th>
                                        <th>{{$data->Ami}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">CCB-nonDHP</th>
                                        <th>{{$data->CCB}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">diltiazem</th>
                                        <th>{{$data->dil}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Verapamil</th>
                                        <th>{{$data->Vera}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">ACEI</th>
                                        <th>{{$data->ACEI}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">ARB</th>
                                        <th>{{$data->ARB}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">digoxin</th>
                                        <th>{{$data->dig}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">betablocker</th>
                                        <th>{{$data->beta}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">diuretics</th>
                                        <th>{{$data->diu}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">statins</th>
                                        <th>{{$data->stat}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">fibrate</th>
                                        <th>{{$data->fibr}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Gingo</th>
                                        <th>{{$data->Gingo}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">thyroxine</th>
                                        <th>{{$data->thyrox}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">antithyroid</th>
                                        <th>{{$data->antithy}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">OAD</th>
                                        <th>{{$data->OAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PPAR-agonist</th>
                                        <th>{{$data->PPAR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Insulin</th>
                                        <th>{{$data->Insul}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">NSAID</th>
                                        <th>{{$data->NSA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">hormone</th>
                                        <th>{{$data->hormon}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">ketaconazole</th>
                                        <th>{{$data->keta}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">rifampin</th>
                                        <th>{{$data->rifa}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">H2_blocker</th>
                                        <th>{{$data->H2}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PPI</th>
                                        <th>{{$data->PPI}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">steroid</th>
                                        <th>{{$data->Steroid}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">alpha_blocker</th>
                                        <th>{{$data->alpha}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">nitrate</th>
                                        <th>{{$data->nitrate}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">propafenone</th>
                                        <th>{{$data->propafe}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">flecainide</th>
                                        <th>{{$data->fleca}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">mexitil</th>
                                        <th>{{$data->mexit}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">NOACs</th>
                                        <th>{{$data->NOAC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">other</th>
                                        <th>{{$data->other}}</th>
                                    </tr>



                                    @endforeach
                                </tbody>
                            </table>


                        </div>
                        <!-- block 4 -->
                        <?php
                    $i=4;
                    $b4_page=0;
                    ?>
                        <div class="container_program_right rf_block_right4_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Family History</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical5 as $key => $data)
                                    <tr>
                                        <th style="width:50%;">Family screening</th>
                                        <th>{{$data->FS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family</th>
                                        <th>{{$data->FI}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Son</th>
                                        <th>{{$data->Son}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Daughter</th>
                                        <th>{{$data->Dau}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family None</th>
                                        <th>{{$data->FDH}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Hypertention Memo</th>
                                        <th>{{$data->HTN}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Diabetes Memo</th>
                                        <th>{{$data->DM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Hyperlipidemia Memo</th>
                                        <th>{{$data->HYP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Thyroid function Memo</th>
                                        <th>{{$data->TF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Immune allergy Memo</th>
                                        <th>{{$data->IA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Tumor Memo</th>
                                        <th>{{$data->FTM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Stroke Memo</th>
                                        <th>{{$data->FSM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family AMI Memo</th>
                                        <th>{{$data->FAM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family CAD Memo</th>
                                        <th>{{$data->FCM}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Arrhythmia Memo</th>
                                        <th>{{$data->FAA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family SCD Memo</th>
                                        <th>{{$data->FSCD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Family Other disease</th>
                                        <th>{{$data->FOD}}</th>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 5 -->
                        <?php
                    $i=5;
                    $b5_page=0;
                    ?>
                        <div class="container_program_right rf_block_right5_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Lab data</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical7 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">PT</th>
                                        <th>{{$data->PT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">aPTT</th>
                                        <th>{{$data->aPTT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">INR</th>
                                        <th>{{$data->INR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">BUN</th>
                                        <th>{{$data->BUN}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Creatinine</th>
                                        <th>{{$data->CRT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">eGFR</th>
                                        <th>{{$data->EGFR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AST</th>
                                        <th>{{$data->AST}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">ALT</th>
                                        <th>{{$data->ALT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">UA</th>
                                        <th>{{$data->UA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">WBC</th>
                                        <th>{{$data->WBC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">HB</th>
                                        <th>{{$data->HB}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PLT</th>
                                        <th>{{$data->PLT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Seq</th>
                                        <th>{{$data->Seq}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">lym</th>
                                        <th>{{$data->lym}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">eosin</th>
                                        <th>{{$data->EOS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">T.Cho</th>
                                        <th>{{$data->TCH}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">HDL</th>
                                        <th>{{$data->HDL}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LDL</th>
                                        <th>{{$data->LDL}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">TG</th>
                                        <th>{{$data->TG}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">FBS</th>
                                        <th>{{$data->FBS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">THS</th>
                                        <th>{{$data->THS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Free T4</th>
                                        <th>{{$data->FT4}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">T3</th>
                                        <th>{{$data->T3}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">T4</th>
                                        <th>{{$data->T4}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 6 -->
                        <?php
                    $i=6;
                    $b6_page=0;
                    ?>
                        <div class="container_program_right rf_block_right6_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">ECG</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical8 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">EKG test date</th>
                                        <th>{{$data->date}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Diagnosis of EKG</th>
                                        <th>{{$data->DIAG}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Rate</th>
                                        <th>{{$data->Rate}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PR interval</th>
                                        <th>{{$data->PR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">QRS interval</th>
                                        <th>{{$data->QRS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">QT interval</th>
                                        <th>{{$data->QT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">QTc</th>
                                        <th>{{$data->QTC}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 7 -->
                        <?php
                    $i=7;
                    $b7_page=0;
                    ?>
                        <div class="container_program_right rf_block_right7_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Echo</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical9 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Echo_test_date</th>
                                        <th>{{$data->Edate}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LA_diameter</th>
                                        <th>{{$data->LA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LVEDD</th>
                                        <th>{{$data->LVE}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LVESD</th>
                                        <th>{{$data->LVES}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Ant_LV_wall_thickness</th>
                                        <th>{{$data->ALWT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Post_LV_thickness</th>
                                        <th>{{$data->PLT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">RVSP</th>
                                        <th>{{$data->RVSP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LVEF</th>
                                        <th>{{$data->LVEF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">DT_decceleration_time</th>
                                        <th>{{$data->DTDT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">E_wave</th>
                                        <th>{{$data->EW}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">A_wave</th>
                                        <th>{{$data->AW}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">E_A_division</th>
                                        <th>{{$data->EAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Aortic_valve</th>
                                        <th>{{$data->AV}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AS</th>
                                        <th>{{$data->pifas}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AR</th>
                                        <th>{{$data->AR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Mitral_valve</th>
                                        <th>{{$data->MV}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">MS</th>
                                        <th>{{$data->MS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">MR</th>
                                        <th>{{$data->MR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Tricuspid_valve</th>
                                        <th>{{$data->TV}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">TS</th>
                                        <th>{{$data->TS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">TR</th>
                                        <th>{{$data->TR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pulmonary_valve</th>
                                        <th>{{$data->PV}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PS</th>
                                        <th>{{$data->PS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">PR</th>
                                        <th>{{$data->PR}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Other diagnosis</th>
                                        <th>{{$data->OD}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 8 -->
                        <?php
                    $i=8;
                    $b8_page=0;
                    ?>
                        <div class="container_program_right rf_block_right8_0">
                            <!--Section1-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 1 Basic rhythm</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical10 as $key => $data)
                                    <tr>
                                        <th style="width:50%;">Basic rhythm</th>
                                        <th>{{$data->BAR}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!--section2-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 2 Bradycardia and pause</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical10 as $key => $data)
                                    <tr>
                                        <th style="width:50%;">Sinus node dysfunction</th>
                                        <th>{{$data->SND}}</th>
                                    </tr>
                                    <tr>

                                        <?php
                                      if ($data->MPD != ''){
                                      $MPD = explode("=",$data->MPD);
                                            $value = $MPD[1];
                                      }
                                      else {
                                            $value = $data->MPD;

                                      }
                                        ?>
                                        <th style="width:50%;">Max pause duration</th>
                                        <th>{{$value}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AVB</th>
                                        <th>{{$data->EOAB}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AVB type</th>
                                        <th>{{$data->AVB}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>



                            <!--section3-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 3 Supraventricula tachycardis</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical10 as $key => $data)


                                    <tr>
                                        <th style="width:50%;">Episodes of SVT</th>
                                        <th>{{$data->EOS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of Burst APC</th>
                                        <th>{{$data->APC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AT</th>
                                        <th>{{$data->EAT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AF</th>
                                        <th>{{$data->EAF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AFL</th>
                                        <th>{{$data->EAFL}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AVNRT</th>
                                        <th>{{$data->EAVNRT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Episodes of AVRT</th>
                                        <th>{{$data->EAVRT}}</th>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                            <!--section4-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 4 Ventricula tachycardia</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical10 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Episode type</th>
                                        <th>{{$data->ET}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Sustained or not</th>
                                        <th>{{$data->SON}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!--section5-->
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Section 5 Pacemaker</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical10 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Pacemaker</th>
                                        <th>{{$data->PK}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pacemaker function</th>
                                        <th>{{$data->PF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Pacemaker type</th>
                                        <th>{{$data->PT}}</th>
                                    </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 9 -->
                        <?php
                    $i=9;
                    $b9_page=0;
                    ?>
                        <div class="container_program_right rf_block_right9_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Thallium scan</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical11 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Ischemia</th>
                                        <th>{{$data->ISC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Ischemia area</th>
                                        <th>{{$data->ISCA}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 10 -->
                        <?php
                    $i=10;
                    $b10_page=0;
                    ?>
                        <div class="container_program_right rf_block_right10_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Thallium scan</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical12 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Ischemia</th>
                                        <th>{{$data->ISC}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">QT interval at peak exercise</th>
                                        <th>{{$data->QTI}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">QTc interval at peak exercise</th>
                                        <th>{{$data->QTc}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Arrrhythmias during exercise</th>
                                        <th>{{$data->ADE}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Attack pattern</th>
                                        <th>{{$data->AAP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Diagnosis of arrhythmias</th>
                                        <th>{{$data->DOA}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 11 -->
                        <?php
                    $i=11;
                    $b11_page=0;
                    ?>
                        <div class="container_program_right rf_block_right11_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">MRI</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical13 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">LVEDD</th>
                                        <th>{{$data->LVEDD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LVESD</th>
                                        <th>{{$data->LVESD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">RVEDD</th>
                                        <th>{{$data->RVEDD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">RVESD</th>
                                        <th>{{$data->RVESD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">RVEF</th>
                                        <th>{{$data->RVEF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">LVEF</th>
                                        <th>{{$data->LVEF}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Late gadolinium enhancement</th>
                                        <th>{{$data->LGE}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Location of LGE</th>
                                        <th>{{$data->LOLGE}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 12 -->
                        <?php
                    $i=12;
                    $b12_page=0;
                    ?>
                        <div class="container_program_right rf_block_right12_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">SAECG</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical14 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Total QRS duration (filtered)</th>
                                        <th>{{$data->TQRS}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Duration of HFLA signals <40 uV</th> <th>{{$data->DHFLA}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">RMS voltage in terminal 40 ms</th>
                                        <th>{{$data->RMS}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 13 -->
                        <?php
                    $i=13;
                    $b13_page=0;
                    ?>
                        <div class="container_program_right rf_block_right13_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Coronary artery angiography</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical15 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">CAD</th>
                                        <th>{{$data->CAD}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Vessels</th>
                                        <th>{{$data->Vessels}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Stenting</th>
                                        <th>{{$data->Stenting}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">other diagnosis</th>
                                        <th>{{$data->OD}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- block 14 -->
                        <?php
                    $i=14;
                    $b14_page=0;
                    ?>
                        <div class="container_program_right rf_block_right14_0">
                            <table class="table table-bordered table-hover table-condensed">
                                <thead>
                                    <th colspan="2" title="Field #1" style="background-color:#ABC3BF ;">Coronary artery angiography</th>
                                </thead>
                                <tbody>
                                    @foreach($case_clinical16 as $key => $data)

                                    <tr>
                                        <th style="width:50%;">Corrected SANRT</th>
                                        <th>{{$data->CSANT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AVN 1 to 1</th>
                                        <th>{{$data->AVN11}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">AVNERP</th>
                                        <th>{{$data->AVNERP}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Inducible VT</th>
                                        <th>{{$data->IVT}}</th>
                                    </tr>
                                    <tr>
                                        <th style="width:50%;">Inducible VF</th>
                                        <th>{{$data->IVF}}</th>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
<!--
						<!-- block 15 -->
                        <?php
                    $i=15;
                    $b15_page=0;
                    ?>
                        <div class="container_program_right rf_block_right15_0">
                        </div>
                        <!-- block 16 -->
                        <?php
                    $i=16;
                    $b16_page=0;
                    ?>
                        <div class="container_program_right rf_block_right16_0">
                        </div>
-->
                    </div>
                </div>
            </div>
        </div>





</section>

<script>
    var i = 1;
    var current_page = 1;
    var block_page = [0,
        <?php echo $b1_page.",".$b2_page;?>
    ];

    $(document).ready(function() {
        $(".container_program_right").hide();
        $("#rfm_right" + i).removeClass("rf-inactive_right").addClass("rf-active_right");
        $(".rf_block_right" + i + "_0").fadeIn(10, "swing");
    });

    $(".rf-item_right").click(function() {
        current_page = 0;
        $("#rfm_right" + i).removeClass("rf-active_right").addClass("rf-inactive_right");

        i = $(this).data('num');
        $("#rfm_right" + i).removeClass("rf-inactive_right").addClass("rf-active_right");


        $(".container_program_right").fadeOut(10, "swing");
        $('.' + $(this).data('rel')).fadeIn(10, "swing");
    });
</script>

@endsection