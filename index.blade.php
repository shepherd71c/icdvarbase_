@extends('frontend.layouts.master')
@section('title', 'Home')
@section('content')
@section('nav_home', 'active')


<script src="https://cdn.jsdelivr.net/npm/vega@4"></script>
<script src="https://cdn.jsdelivr.net/npm/vega-lite@2"></script>
<script src="https://cdn.jsdelivr.net/npm/vega-embed@3"></script>
<div class="container-in">
    <div class="container-fluid">
        <div class="row">
            <div class="col-8">
                <div class="blockss" style="padding-top:3%;padding-bottom:2%;">
                    <p>
					The advancement of genetic testing by next-generation sequencing opens the gate for the prevention, diagnosis, management, and risk stratification of inherited cardiac diseases (ICDs). However, the translational gap from genetic information to clinical practice remains significant due to complex and overwhelming information from genetics and phenotype of ICDs are hardly integrated.<br>
					<br>
					An ICDscreening panel that embraced the multigene sequencing, genetic variant analysis, and Sanger sequencing validation was developed for the coding regions of the 12 key disease-related genes for five types of ICDs. Genetic information was further incorporated with clinical data of ICDs to establish ICDs Genetic Variant Database (ICDVarBase).<br>
					<br>
					Genetic analysis was performed for 100 individuals, including patients with ICDs and their relatives. Fifty-three putative variants were identified. The analysis results and patients' clinical information were integrated in ICDVarBase, which comprehensively presented the link between genotype and phenotypes in multiple aspects. Pathogenicity classification by ACMG criteria can be altered after the domestic genome database was included.<br>
					<br>
					ICDVarBase is the first integrative database of ICDs to provide integrative analysis of genotypes and phenotypes. This would provide new aspects to interpret the clinical effect and pathogenicity of mutant genes.<br>
                    </p>
                </div>
                <!--
                <div class="blockss" style="margin-top:5%;">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block w-100" src="img/suddendeath.jpg" alt="First slide" style="height:500px;">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/cardiac_arrest_cpr.jpg" alt="Third slide" style="height:500px;">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block w-100" src="img/man-having-heart-attack.jpg" alt="Third slide" style="height:500px;">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
              -->
            </div>
            <div class="col-4">
                <div class="rightmenu">
                    <h1>Quick Start</h1>
                    <div class="rightmenu_quick">

                        <div class="sum">
                            <?php echo $samples_bar ?>
                        </div>&nbsp;<span>samples, </span><br>

                        <div class="sum">
                            <?php echo $vari_bar ?>
                        </div>&nbsp;<span>variants identified from </span>
                        <div class="sum">
                            <?php echo $countss_bar ?>
                        </div>&nbsp;<span>Taiwanese.</span>

                    </div>
                    <!--
                    <h1>Citation</h1>
                    &nbsp;
                    <p>» <strong>Yu-Feng Hu</strong> M.D., Ph.D. </br>
                        Email :&nbsp;&nbsp;<a href="mailto:yfhu@vghtpe.gov.tw">yfhu@vghtpe.gov.tw </a></p>

                    <p>» <strong>Hsin-Tzu huang</strong> Ph.D. candidate</br>
                        Email :&nbsp;&nbsp;<a href="mailto:hsintzu@g2.nctu.edu.tw">hsintzu@g2.nctu.edu.tw </a></p>
                    -->
                    &nbsp;
                    <h1>Lastest news</h1>
                    &nbsp;
                    <div class="version">
                        <p>» Release 1.0.1 at 20181010</p>
<!--
                        <p>» Release 1.0.2 at 20181113</p>
                        <p>» Release 1.0.3 at 20190116</p>
                        <p>» Release 1.0.4 at 20190218</p>
-->
						<p>» Release 1.1.0 at 20190825</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
<!--
	<div class="container-in">
        <div class="container-fluid" style="height:auto;">
            <div class="row">
                <div class="col-12">
                    <div class="blockss" style=" margin-bottom:0;">
                        <h2>Radial Tree plot of ICDVarBase</h2>
                        <hr>
                        </hr>
                        <br>
                        <img src="img/tree.jpg" alt="" height="auto" width="100%;">
                        <br>
                        <br>
                        <p>64 putative pathogenic variants (two pathogenic, two likely pathogenic and sixty uncertain significance) were identified from the 94 out of 165 Taiwanese individuals.</p>
                    </div>
-->
                    <!--
                    <div id="vis"></div>
                    -->
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="blockss">
                    <h2>The workflow of ICDscreening</h2>
                    <hr>
                    </hr>
                    <img src="img/protocal2.jpg" alt="">
                    <br>
                    <br>
                    <br>

                    <p> ICDscreening embraces the multi-gene sequencing panel, genetic variants analysis (ICDpipeline) and Sanger sequencing validation. The significance of variants was abided by the American College of Medical Genetics and Genomics
                        (ACMG) guideline.
                    </p>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="blockss">
                    <h2>Disease-associated genes</h2>
                    <hr>
                    </hr>
                    <br><br>
                    <table class="table-hover">
                        <thead style="border-top: solid 3.6px black;border-bottom:  solid 3.6px black;">
                            <tr>
                                <td style="min-width:24vw;font-size:1.2rem;">Inherited cardiac diseases&nbsp;</td>
                                <td style="min-width:16vw;font-size:1.2rem;">Gene&nbsp;</td>
                            </tr>
                        </thead>

                        <tbody>

                            <tr style="border-bottom: solid 1px black;">
                                <td>LQTS&nbsp;</td>
                                <td style=" font-style:italic;">
                                    <p>KCNQ1</p>
                                    <p>KCNH2</p>
                                    <p>SCN5A</p>
                                    <p>KCNE1</p>
                                    <p>KCNE2</p>
                                    <p>CACNA1C&nbsp;</p>
                                </td>
                            </tr>
                            <tr style="border-bottom: solid 1px black;">
                                <td>BrS&nbsp;</td>
                                <td style="font-style:italic;">
                                    <p>SCN5A</p>
                                    <p>CACNA1C</p>
                                    <p>CACNB2</p>
                                    <p>KCNH2&nbsp;</p>
                                </td>
                            </tr>
                            <tr style="border-bottom: solid 1px black;">
                                <td>CPVT&nbsp;</td>
                                <td style="font-style:italic;">RYR2&nbsp;</td>
                            </tr>
                            <tr style="border-bottom: solid 1px black;">
                                <td>ARVC&nbsp;</td>
                                <td style="font-style:italic;">
                                    <p>PKP2</p>
                                    <p>DSP&nbsp;</p>
                                </td>
                            </tr>
                            <tr style="border-bottom: solid 1px black;">
                                <td>HCM&nbsp;</td>
                                <td style="font-style:italic;">
                                    <p>MYBPC3</p>
                                    <p>MYH7&nbsp;</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="blockss">
                    <!--
                    <script type="text/javascript">
                        const array1 = "{{$radial_plot}}"
                    </script>
-->
                    <br>
                    <p> ICDscreening panel was designed to target the coding regions of the 12 key disease-related genes for 5 types of ICD.
                    </p>
                    <script type="text/javascript" src="{{url('/')}}/js/Radial_tree.js"></script>
                </div>
            </div>
        </div>
    </div>



</div>

@endsection