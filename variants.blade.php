@extends('frontend.layouts.master')
@section('title', 'Browse')
@section('content')

<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">


                <div class="rf-form_list_block">
                    <ul style="margin-bottom:0;    border-bottom: #566457 4px solid;">
                        <li>
                            <div class="rf-item" data-num="1" data-rel="rf_block1_0" style="cursor: auto; ">
                                <div id="rfm1" class="rf-active">{{$variants}}</div>
                            </div>
                        </li>
                    </ul>
                    <div class="container_program rf_block1_0 rf-form_list_block_right">
                        <div class="container_it" id="variant_info" style="margin-top:0;">
                            <table class="table table-bordered table-hover table-condensed td_left" style="border:0px;margin:auto;">
                                <tbody>
                                    @foreach($variants_info as $key => $data)
                                    <tr>
                                        <td colspan="8" class="td_title">Information of variant</td>
                                    </tr>
                                    <tr>
                                        <td>Gene</td>
                                        <td colspan="7" style="font-weight:bold;font-style:italic">{{$data->gene}}</td>
                                    </tr>
                                    <tr>
                                        <td>Variantion</td>
                                        <td colspan="7">
                                            <form action="{{url('variants')}}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="name" value={{$data->variants}}>
                                                <button type="submit" name="button">{{$data->variants}}</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Variant type</td>
                                        <td colspan="7">{{$data->mutation_type}}</td>
                                    </tr>

                                    <?php $a = explode("|",$data->annotation);
                                          $c = explode("&",$a[2]);
                                          $b = explode("&",$data->annotation);
                                        ?>
                                    <tr>
                                        <td>Isoform</td>
                                        <td colspan="7">{{$a[0]}} | {{$a[1]}}</td>
                                    </tr>
                                    <tr>
                                        <td>Exon #</td>
                                        <td colspan="7">
                                            <?php
                                            if(isset($c[0]) & $c[0]!="NA"){
                                            echo $c[0];
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>dbSNP rsid</td>
                                        <td colspan="7">
                                            <?php
                                            if(isset($b[2]) & $b[2]!="NA"){

                                            ?>
                                            <a href="https://www.ncbi.nlm.nih.gov/snp/{{$b[2]}}">{{$b[2]}}</a></th>
                                            <?php
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Clinvar classification</td>
                                        <td colspan="7">
                                            <?php
                                          if(isset($b[6])){
                                          echo $b[6];
                                          }
                                          else {
                                          echo "-";
                                          }
                                          ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PolyPhen2</td>
                                        <td colspan="7">
                                            <?php
                                        if(isset($b[4])){
                                          echo $b[4];
                                        }
                                        else {
                                          echo "";
                                        }
                                          ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SIFT</td>
                                        <td colspan="7">
                                            <?php
                                          if(isset($b[5])){
                                            echo $b[5];
                                          }
                                          else {
                                            echo "";
                                          }
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="8" class="td_title">Genotype frequency in different population</td>
                                    </tr>
                                    <tr>
                                        <td>Genotype</td>
                                        <td># of patients in the database </td>
                                        <td>Taiwan Biobank (1504)</td>
                                        <td>AMR (-)</td>
                                        <td>EUR (-)</td>
                                        <td>AFR (-)</td>
                                        <td>SAS (-)</td>
                                        <td>EAS (-)</td>
                                    </tr>
                                    <?php

                                    $tai = explode(":",$data->TaiwanBioBank);

                                    for($i=3; $i<12; $i+=2){
                                      if(isset($a[$i])){
                                        $tmp = explode(",",$a[$i]);


                                        for($y=0; $y<(count($tmp)-1); $y++){
                                          if(isset($tmp[$y])){
                                            $end = explode(":",$tmp[$y]);
                                            if(isset($end[1])){
                                              if ($end[0] = $tai[0]){
                                                $value_of_rate[$i] = round($end[1],4);
                                              }
                                              else {
                                                $value_of_rate[$i] = "-";
                                              }
                                            }
                                            else{
                                                  $value_of_rate[$i] = "-";
                                            }
                                          }
                                          else {
                                              $value_of_rate[$i] = "-";
                                          }
                                        }
                                      }
                                      else {
                                          $value_of_rate[$i] = "-";
                                      }
                                    }

                                    ?>
                                    <tr>
                                        <td>{{$tai[0]}}</td>
                                        <td>{{$variants_num}}</td>
                                        <td>
                                            <?php
                                              if($tai[1] != '-'){
                                              echo (float)$tai[1]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $tai[1];
                                              }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              if($value_of_rate[3] != '-'){
                                              echo (float)$value_of_rate[3]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $value_of_rate[3];
                                              }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              if($value_of_rate[5] != '-'){
                                              echo (float)$value_of_rate[5]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $value_of_rate[5];
                                              }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              if($value_of_rate[7] != '-'){
                                              echo (float)$value_of_rate[7]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $value_of_rate[7];
                                              }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              if($value_of_rate[9] != '-'){
                                              echo (float)$value_of_rate[9]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $value_of_rate[9];
                                              }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                              if($value_of_rate[11] != '-'){
                                              echo (float)$value_of_rate[11]*100;
                                              echo '%';
                                              }
                                              else {
                                              echo $value_of_rate[11];
                                              }
                                            ?>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td colspan="8" class="td_title">ACMG criteria without family information (Evidence of pathogenicity)</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Very Strong</td>
                                        <td colspan="2">Strong</td>
                                        <td colspan="2">Moderate</td>
                                        <td colspan="2">Supporting</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <?php
                                            if(isset($data->VSRule) & $data->VSRule!="NA"){
                                            echo $data->VSRule;
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                        <td colspan="2">
                                            <?php
                                            if(isset($data->STRule) & $data->STRule!="NA"){
                                            echo $data->STRule;
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                        <td colspan="2">
                                            <?php
                                            if(isset($data->ModRule) & $data->ModRule!="NA"){
                                            echo $data->ModRule;
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                        <td colspan="2">
                                            <?php
                                            if(isset($data->SuppRule) & $data->SuppRule!="NA"){
                                            echo $data->SuppRule;
                                            }
                                            else {
                                            echo "-";
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" class="td_title">Information of patients in the database</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="2">Case number</td>
                                        <!--
                                        <td>Family #</td>
                                        <td colspan="2">Clinical diagnosis or familyship </td>
                                      -->
                                        <td colspan="2">Genotype frequency (Taiwan Biobank)</td>
                                        <td colspan="2">ACMG criteria</td>
                                        <td colspan="2">ACMG classification</td>
                                    </tr>
                                    @foreach ($variants2 as $key => $value)
                                    <tr>
                                        <td colspan="2">
                                            <form action="{{url('Browse_case')}}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="name" value={{$value->alley_name}}>
                                                <button type="submit" name="button">{{$value->alley_name}}</button>
                                            </form>
                                        </td>
                                        <!--<td>{{$value->Family_Barcode}}</td>
                                        <td colspan="2">{{$value->Disease}}</td>-->
                                        <td colspan="2">
                                            <?php
                                          $tai2 = explode(":",$value->TaiwanBioBank);
                                          if($tai2[1] != "-"){
                                            echo $tai[0],":";
                                            echo (float)$tai2[1]*100;
                                            echo "%";
                                          }
                                           else {
                                             echo $value->TaiwanBioBank;

                                           }
                                           ?>
                                        </td>
                                        <td colspan="2">{{$value->ACMG_Rule}}

                                            <?php
/*
                                        $tai3 = explode("|",$value->ACMG_Rule);
                                        for ($i=0; $i < 3 ; $i++) {
                                            if($tai3[$i] != "NA"){
                                              echo $tai3[$i]+','
                                        }
*/
                                         ?>
                                        </td>
                                        <td colspan="2">{{$value->ACMG_classify}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection