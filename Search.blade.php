@extends('frontend.layouts.master')
@section('title', 'Search')
@section('content')
@section('nav_Search', 'active')

<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">
                <!--
                <div class="rf-form_list_block">
                    <ul style="margin-bottom:0;">
                        <li>
                            <div class="rf-item" data-num="1" data-rel="rf_block1_0">
                                <div id="rfm1" class="rf-inactive">Advanced Search</div>
                            </div>
                        </li>
                        <li>
                            <div class="rf-item" data-num="2" data-rel="rf_block2_0">
                                <div id="rfm2" class="rf-inactive">Clinical Search</div>
                            </div>
                        </li>
                    </ul>
                </div>
              -->
                <!-- block 1 -->
                <?php
                $i=1;
                $b1_page=0;
                ?>

                <div class="container_program rf_block1_0">
                    <form action="{{url('Search_sample')}}" method="post" name="form1">
                        {{ csrf_field() }}

                        <div class="ad_search">

                            <div class="wrappp">


                                <h3>Please select the disease</h3>
                                <select name="disease_" class="pretty_select">

                                    @foreach ($disease_ as $key => $data)
                                    <option value='{{$data->Disease}}'>{{$data->Disease}}</option>
                                    @endforeach

                                    <option value="no_d" selected>-- show all --</option>
                                </select>

                            </div>

                            <div class="wrappp">


                                <h3>Please select the gene</h3>

                                <select name="gene_" class="pretty_select">

                                    @foreach ($gene_ as $key => $data)
                                    <option value='{{$data->gene}}'>{{$data->gene}}</option>
                                    @endforeach

                                    <option value="no_g" selected>-- show all --</option>
                                </select>
                                <br>
                                <br>


                            </div>

                            <div class="wrappp">


                                <h3>Please select the variants</h3>

                                <select name="variants_" class="pretty_select">

                                    @foreach ($variants_ as $key => $data)
                                    <option value='{{$data->variants}}'>{{$data->variants}} ({{$data->gene}})</option>
                                    @endforeach

                                    <option value="no_v" selected>-- show all --</option>
                                </select>
                                <br>
                                <br>

                            </div>

                            <div class="wrappp">


                                <h3>Please select the sample ID</h3>

                                <select name="sample_" class="pretty_select">

                                    @foreach ($sample_ as $key => $data)
                                    <option value='{{$data->Barcode}}'>{{$data->alley_name}}</option>
                                    @endforeach

                                    <option value="no_n" selected>-- show all --</option>

                                </select>


                            </div>

                            <div class="wrappp">

                                <h3>Please select the reported classification</h3>

                                <select name="path_" class="pretty_select">
                                    <option value='Pathogenic'>Pathogenic</option>
                                    <option value='Likely pathogenic'>Likely pathogenic</option>
                                    <option value='Uncertain significance'>Uncertain significance</option>
                                    <option value="no_c" selected>-- show all --</option>
                                </select>

                            </div>

                        </div>

                        <input type="submit" class="submit" value="submit">
                        <!--
                        <div class="ad_search">


                            <table style="width:80%;text-align: center;margin:auto;">
                                <tbody>
                                    <tr>
                                        <td class="show_">
                                            <input type="radio" name="show" value="sample" onclick="calA();" checked> show with sample list<br>

                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../public/img/bycase.png" alt="" style="width:100%;">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="show_">
                                            <input type="radio" name="show" value="varients" onclick="calB();"> show with varients list<br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../public/img/byvariants.png" alt="" style="width:100%;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height:15vh;">
                                            <input type="submit" class="submit">

                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>
                      -->
                    </form>

                    <!--
                    <script>
                        function calA() {
                            document.form1.action = "{{url('Search_sample')}}";
                        }

                        function calB() {
                            document.form1.action = "{{url('Search_variants')}}";
                        }
                    </script>
-->
                    <?php /*
  @foreach($petani as $key => $data)
      <tr>
        <th>{{$data->Depth}}</th>

        <th>{{$data->TaiwanBioBank}}</th>
        <th>{{$data->Barcode}}</th>
      </tr>
  @endforeach
  */ ?>

                </div>
                <!-- block 2 -->
                <?php
                $i=2;
                $b2_page=0;
                /*
                ?>

                <div class="container_program rf_block2_0">



                    <form action="{{url('Search_clinical')}}" method="post" name="form2">
                        {{ csrf_field() }}
                        <div class="select_section" id="section1_">
                            <h2>General Data</h2>

                            <!-- age -->
                            <!--
                            <h3>Age</h3>
                            <input type="radio" value="%" checked="checked" name="age"> all<br>
                            <input type="radio" value="young" name="age"> under 50<br>
                            <input type="radio" value="old" name="age"> above 50<br>
                          -->
                            <br>
                            <!-- gender -->
                            <h3>Gender</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="sex"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="sex"> male<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="sex"> female<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="sex"> none on record<br></span>

                            <!-- height -->
                            <hr>
                            <h3>Height record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="height"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="height"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="height"> no<br></span>
                            <!-- weight -->
                            <hr>
                            <h3>Weight record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="weight"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="weight"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="weight"> no<br></span>

                            <!-- WC -->
                            <hr>
                            <h3>Waistline record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="WC"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="WC"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="WC"> no<br></span>

                            <!-- SBP -->
                            <hr>
                            <h3>Systolic blood pressure record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="SBP"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="SBP"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="SBP"> no<br></span>

                            <!-- DBP -->
                            <hr>
                            <h3>Diastolic blood pressure record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="DBP"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="DBP"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="DBP"> no<br></span>

                            <!-- PR -->
                            <hr>
                            <h3>Pulse record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="PR"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="PR"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="PR"> no<br></span>

                            <!-- smoke -->
                            <hr>
                            <h3>Smoking or not</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="smoke"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="smoke"> smoker<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="smoke"> ex-smoker<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="smoke"> non_smoker<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="smoke"> none on record<br></span>

                            <!-- alcohol -->
                            <hr>
                            <h3>Drink alcohol or not</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="alcohol"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="alcohol"> alcohol consumption<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="alcohol"> no alcohol consumption<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="alcohol"> none on record<br></span>

                            <!-- Vegeteriany -->
                            <hr>
                            <h3>Vegeteriany or not</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="vegeteriany"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="vegeteriany"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="vegeteriany"> sometimes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="vegeteriany"> no<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="vegeteriany"> none on record<br></span>

                        </div>
                        <br>

                        <div class="select_section" id="section2_">
                            <h2>Diagnosis of Cardiology</h2></span>
                            <!-- DIAG -->
                            <br>
                            <h3>Diagonisis of heart disease</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="DIAG"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="LongQT" name="DIAG"> LongQT<br></span>
                            <span class="checkboxcss"><input type="radio" value="BrS" name="DIAG"> BrS<br></span>
                            <span class="checkboxcss"><input type="radio" value="CPVT" name="DIAG"> CPVT<br></span>
                            <span class="checkboxcss"><input type="radio" value="ARVC" name="DIAG"> ARVC<br></span>
                            <span class="checkboxcss"><input type="radio" value="HCM" name="DIAG"> HCM<br></span>

                            <!-- Gene_test -->
                            <hr>
                            <h3>Gene test or not</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Gene_test"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Gene_test"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Gene_test"> no<br></span>

                        </div>
                        <br>
                        <div class="select_section" id="section3_">

                            <h2>Medical of History</h2></span>
                            <!-- Thromoboembolism_disease -->
                            <br>
                            <h3>Thromoboembolism disease</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Thromoboembolism_disease"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Thromoboembolism_disease"> Stoke<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="Thromoboembolism_disease"> TIA<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="Thromoboembolism_disease"> Systemic embolism<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Thromoboembolism_disease"> no<br></span>

                            <!-- gastronintestinal_bleeding -->
                            <hr>
                            <h3>Gastronintestinal bleeding record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="gastronintestinal_bleeding"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="gastronintestinal_bleeding"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="gastronintestinal_bleeding"> no<br></span>

                            <!-- Hypertention -->
                            <hr>
                            <h3>Hypertention record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Hypertention"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Hypertention"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Hypertention"> no<br></span>

                            <!-- Diabetes -->
                            <hr>
                            <h3>Diabetes record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Diabetes"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Diabetes"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Diabetes"> no<br></span>

                            <!-- Heart_failure -->
                            <hr>
                            <h3>Heart failure record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Heart_failure"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Heart_failure"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Heart_failure"> no<br></span>

                            <!-- Hyperlipidemia -->
                            <hr>
                            <h3>Hyperlipidemia record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Hyperlipidemia"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Hyperlipidemia"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Hyperlipidemia"> no<br></span>

                            <!-- AMI -->
                            <hr>
                            <h3>AMI record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="AMI"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="AMI"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="AMI"> no<br></span>

                            <!-- PAOD -->
                            <hr>
                            <h3>PAOD record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="PAOD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="PAOD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="PAOD"> no<br></span>

                            <!-- CAD -->
                            <hr>
                            <h3>CAD record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="CAD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="CAD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="CAD"> no<br></span>

                            <!-- CAD_vessel_LAD -->
                            <hr>
                            <h3>CAD vessel LAD record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="CAD_vessel_LAD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="CAD_vessel_LAD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="CAD_vessel_LAD"> no<br></span>

                            <!-- CAD_vessel_RCA -->
                            <hr>
                            <h3>CAD vessel RCA record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="CAD_vessel_RCA"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="CAD_vessel_RCA"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="CAD_vessel_RCA"> no<br></span>

                            <!-- CAD_vessel_LCX -->
                            <hr>
                            <h3>CAD vessel LCX record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="CAD_vessel_LCX"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="CAD_vessel_LCX"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="CAD_vessel_LCX"> no<br></span>

                            <!-- Heart_valve_disease -->
                            <hr>
                            <h3>Heart valve disease record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Heart_valve_disease"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Heart_valve_disease"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Heart_valve_disease"> no<br></span>

                            <!-- Other Arrhythmia -->
                            <hr>
                            <h3>Other Arrhythmia record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Other_Arrhythmia"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Other_Arrhythmia"> None<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Other_Arrhythmia"> AF<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="Other_Arrhythmia"> WPW<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="Other_Arrhythmia"> ANRT<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="Other_Arrhythmia"> PSVT<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="Other_Arrhythmia"> VT<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="Other_Arrhythmia"> other<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="Other_Arrhythmia"> no record<br></span>


                            <!-- Congenital_heart_disease -->
                            <hr>
                            <h3>Congenital heart disease record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Congenital_heart_disease"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Congenital_heart_disease"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Congenital_heart_disease"> no<br></span>

                            <!-- Pacemaker -->
                            <hr>
                            <h3>Pacemaker record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Pacemaker"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Pacemaker"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Pacemaker"> no<br></span>

                            <!-- Thyroid_function -->
                            <hr>
                            <h3>Thyroid function record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Thyroid_function"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Thyroid_function"> Hyperthyroidism<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="Thyroid_function"> Hypothyroidism<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Thyroid_function"> normal<br></span>

                            <!-- COPD -->
                            <hr>
                            <h3>COPD record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="COPD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="COPD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="COPD"> no<br></span>

                            <!-- Liver_disease -->
                            <hr>
                            <h3>Liver disease record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Liver_disease"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Liver_disease"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Liver_disease"> no<br></span>

                            <!-- CKD -->
                            <hr>
                            <h3>Chronic kidney disease (CKD) record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="CKD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="CKD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="CKD"> no<br></span>

                            <!-- Sleep apnea -->
                            <hr>
                            <h3>Sleep apnea record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Sleep_apnea"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Sleep_apnea"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Sleep_apnea"> no<br></span>

                            <!-- Syncope -->
                            <hr>
                            <h3>Syncope record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Syncope"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Syncope"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Syncope"> no<br></span>

                            <!-- mental_illness -->
                            <hr>
                            <h3>Mental illness record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="mental_illness"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="mental_illness"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="mental_illness"> no<br></span>

                            <!-- Drug_abuse -->
                            <hr>
                            <h3>Drug abuse record</h3>
                            <span class="checkboxcss"><input type="radio" value="all" checked="checked" name="Drug_abuse"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="yes" name="Drug_abuse"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="no" name="Drug_abuse"> no<br></span>
                        </div>
                        <br>
                        <div class="select_section" id="section4_">
                            <h2>Cardiovascular Agents Intake</h2></span>
                            <!-- antiplatelet -->
                            <br>
                            <h3>Antiplatelet record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="antiplatelet"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="antiplatelet"> bokey (Asprin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="antiplatelet"> plavix (Clopidogrel)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="antiplatelet"> licodin, Ticlid (Ticlopidine HCl)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="antiplatelet"> Agrylin (Anagrelide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="antiplatelet"> Brilinta (Ticagrelor)<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="antiplatelet"> Aggrastat (Tirofiban)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="antiplatelet"> no<br></span>

                            <!-- pleetal -->
                            <hr>
                            <h3>Pleetal record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="pleetal"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="pleetal"> Plestar, Pletaal (Cilostazol)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="pleetal"> no<br></span>

                            <!-- Dronedarone -->
                            <hr>
                            <h3>Dronedarone record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Dronedarone"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Dronedarone"> Cordarone<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Dronedarone"> no<br></span>

                            <!-- Amiodarone -->
                            <hr>
                            <h3>Amiodarone record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Amiodarone"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Amiodarone"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Amiodarone"> no<br></span>

                            <!-- CCB-nonDHP -->
                            <hr>
                            <h3>CCB-nonDHP record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="CCB-nonDHP"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="CCB-nonDHP"> Norvasc, Nobar (Amlodipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="CCB-nonDHP"> Coniel (Benidipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="CCB-nonDHP"> Caduet<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="CCB-nonDHP"> Plendil (Felodipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="CCB-nonDHP"> Lacipil, Lasyn (Lacidipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="CCB-nonDHP"> Zanidip (Lercanidipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="8" name="CCB-nonDHP"> Perdipine (Nicardipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="9" name="CCB-nonDHP"> Adalat, Atanaal, Coracten, Nifecardia, Towaarat (Nifedipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="10" name="CCB-nonDHP"> Nimotop (Nimodipine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="CCB-nonDHP"> no<br></span>

                            <!-- diltiazem -->
                            <hr>
                            <h3>Diltiazem record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="diltiazem"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="diltiazem"> Cardizem<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="diltiazem"> Herbesser<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="diltiazem"> Progor<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="diltiazem"> no<br></span>

                            <!-- Verapamil -->
                            <hr>
                            <h3>Verapamil record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Verapamil"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Verapamil"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Verapamil"> no<br></span>

                            <!-- ACEI -->
                            <hr>
                            <h3>ACEI record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="ACEI"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="ACEI"> Apo-Capto, Captopri(Captopril)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="ACEI"> Tanatril (Imidapril)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="ACEI"> Zestril (Lisinopril)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="ACEI"> Acertil (Perindopril arginine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="ACEI"> Tritace (Ramipril)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="ACEI"> no<br></span>

                            <!-- ARB -->
                            <hr>
                            <h3>ARB record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="ARB"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="ARB"> Edarbi (Azilsartan)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="ARB"> Coaprovel<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="ARB"> Aprovel (Irbesartan)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="ARB"> Cozaar (Losartan)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="ARB"> Hyzaar<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="ARB"> Olbetec (Olmesartan)<br></span>
                            <span class="checkboxcss"><input type="radio" value="8" name="ARB"> Diovan (Valsartan)<br></span>
                            <span class="checkboxcss"><input type="radio" value="9" name="ARB"> Co-Diovan<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="ARB"> no<br></span>

                            <!-- digoxin -->
                            <hr>
                            <h3>Digoxin record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="digoxin"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="digoxin"> Cardiacin<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="digoxin"> Digosin<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="digoxin"> Lanoxin<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="digoxin"> no<br></span>

                            <!-- betablocker -->
                            <hr>
                            <h3>Pulse record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="betablocker"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="betablocker"> Sectral (Acebutolol)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="betablocker"> Ateol, Mirobect, Tenormin (Atenolol)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="betablocker"> Concor (Bisoprolol)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="betablocker"> Betaloc ZOK (Metoprolol)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="betablocker"> Cardolol, Inderal (Propranolol HCl)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="betablocker"> no<br></span>


                            <!-- diuretics -->
                            <hr>
                            <h3>Diuretics record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="diuretics"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="diuretics"> Dichlortride (Hydrochlorothiazide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="diuretics"> Natrilix (Indapamide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="diuretics"> Fluitran (Trichlormethiazide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="diuretics"> Burinex (Bumetanide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="diuretics"> Fumide, Lasix, Rosis (Furosemide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="diuretics"> Inspra (Eplerenone)<br></span>
                            <span class="checkboxcss"><input type="radio" value="8" name="diuretics"> Aldactone-A, Spirotone XE (Spironolactone)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="diuretics"> no<br></span>


                            <!-- statins -->
                            <hr>
                            <h3>Statins record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="statins"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="statins"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="statins"> no<br></span>

                            <!-- fibrate -->
                            <hr>
                            <h3>Fibrate record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="fibrate"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="fibrate"> Lipitor (Atorvastatin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="fibrate"> Lescol, Lescol XL (Fluvastatin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="fibrate"> Livalo (Pitavastatin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="fibrate"> Mevalotin Protect, U-Prava (Pravastatin sodium)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="fibrate"> Crestor (Rosuvastatin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="fibrate"> no<br></span>

                            <!-- Gingo -->
                            <hr>
                            <h3>Gingo record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Gingo"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Gingo"> Giko<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Gingo"> no<br></span>

                            <!-- thyroxine -->
                            <hr>
                            <h3>Thyroxine record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="thyroxine"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="thyroxine"> Eltroxin, Thyroxine-L, T4 (Levothyroxine sodium)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="thyroxine"> no<br></span>

                            <!-- antithyroid -->
                            <hr>
                            <h3>Antithyroid record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="antithyroid"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="antithyroid"> Carbizo, Neo-thyreostat, Newmazole (Carbimazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="antithyroid"> Lugol's Soln (Iodine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="antithyroid"> Procil (Propylthiouracil)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="antithyroid"> no<br></span>

                            <!-- OAD -->
                            <hr>
                            <h3>OAD record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="OAD"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="OAD"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="OAD"> no<br></span>

                            <!-- PPAR-agonist -->
                            <hr>
                            <h3>PPAR-agonist record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="PPAR-agonist"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="PPAR-agonist"> Bezalip (Bezafibrate)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="PPAR-agonist"> Fenolip, Lipanthyl, Trilipix (Fenofibrate)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="PPAR-agonist"> no<br></span>

                            <!-- Insulin -->
                            <hr>
                            <h3>Insulin record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Insulin"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Insulin"> NovoRapid FlexPen (Insulin Aspart)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="Insulin"> NovoMix 30 FlexPen (Insulin Aspart and Insulin Aspart Protamin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="Insulin"> Levemir FlexPen (Insulin detemir)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="Insulin"> Lantus (Insulin Glargine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="Insulin"> Apidra (Insulin Glulisine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="Insulin"> Humalog (Insulin Lispro and Insulin Lispro Protamine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="8" name="Insulin"> Actrapid (Insulin Regular)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Insulin"> no<br></span>

                            <!-- NSAID -->
                            <hr>
                            <h3>NSAID record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="NSAID"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="NSAID"> Tonec (Aceclofenac)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="NSAID"> Bokey (Aspirin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="NSAID"> Cataflam, Diclofen, Dicloren, Flamquit, Flogofenac, Meitifen, Voltaren, Voren, Votan (Diclofenac)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="NSAID"> Lacoxa, Lonine (Etodolac)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="NSAID"> Melicam, Mobic, Mopik (Meloxicam)<br></span>
                            <span class="checkboxcss"><input type="radio" value="7" name="NSAID"> Genuproxin, Naposin, Napton (Naproxen)<br></span>
                            <span class="checkboxcss"><input type="radio" value="8" name="NSAID"> Celebrex (Celecoxib)<br></span>
                            <span class="checkboxcss"><input type="radio" value="9" name="NSAID"> Arcoxia (Etoricoxib)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="NSAID"> no<br></span>

                            <!-- hormone -->
                            <hr>
                            <h3>Hormone record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="hormone"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="hormone"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="hormone"> no<br></span>

                            <!-- ketaconazole -->
                            <hr>
                            <h3>Ketaconazole record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="ketaconazole"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="ketaconazole"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="ketaconazole"> no<br></span>

                            <!-- rifampin -->
                            <hr>
                            <h3>Rifampin record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="rifampin"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="rifampin"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="rifampin"> no<br></span>

                            <!-- H2_blocker -->
                            <hr>
                            <h3>H2_blocker record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="H2_blocker"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="H2_blocker"> Cimewill, Defense, Tagamet (Cimetidine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="H2_blocker"> Fadin, Famo, Gaster, Supertidine (Famotidien)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="H2_blocker"> Nicewe, Ulsafe, Zantac (Ranitidine)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="H2_blocker"> no<br></span>

                            <!-- PPI -->
                            <hr>
                            <h3>PPI record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="PPI"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="PPI"> Dexilant (Dexlansoprazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="PPI"> Nexium (Esomeprazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="PPI"> Takepron (Lansoprazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="PPI"> Pantoloc (Pantoprazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="6" name="PPI"> Pariet (Rabeprazole)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="PPI"> no<br></span>

                            <!-- steroid -->
                            <hr>
                            <h3>Steroid record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="steroid"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="steroid"> Cortisone<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="steroid"> Decadron, Decone, Dethasone, Dexan, Dexazone, Dorison, Methasone (Dexamethasone)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="steroid"> Solu-sortef (Hydrocortison sodium succinate)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="steroid"> no<br></span>

                            <!-- alpha_blocker -->
                            <hr>
                            <h3>Alpha blocker record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="alpha_blocker"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="alpha_blocker"> Dophilin, Doxaben (Doxazosin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="alpha_blocker"> Hytrin, Telowsin, Tezopin (Terazosin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="alpha_blocker"> no<br></span>

                            <!-- nitrate -->
                            <hr>
                            <h3>Nitrate record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="nitrate"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="nitrate"> Angidil, Isoket,IsSorbitrate (Isosorbide dinitrate)<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="nitrate"> Imdur CR, Ismo 20, Isormol (Isosorbide-5-mononitrate)<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="nitrate"> NTG,Millisrol, Nitrostat, Nitroderm TTS 5, Tridil (Nitroglycerin)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="nitrate"> no<br></span>

                            <!-- propafenone -->
                            <hr>
                            <h3>Propafenone record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="propafenone"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="propafenone"> Rytmonorm (Propafenone)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="propafenone"> no<br></span>

                            <!-- flecainide -->
                            <hr>
                            <h3>Flecainide record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="flecainide"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="flecainide"> Tambocor (Flecainide)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="flecainide"> no<br></span>

                            <!-- mexitil -->
                            <hr>
                            <h3>Mexitil record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="mexitil"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="mexitil"> Mexitil, Meletin (Mexiletine HCl)<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="mexitil"> no<br></span>

                            <!-- NOACs -->
                            <hr>
                            <h3>NOACs record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="NOACs"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="NOACs"> Dabigatra<br></span>
                            <span class="checkboxcss"><input type="radio" value="3" name="NOACs"> Rivoroxaban<br></span>
                            <span class="checkboxcss"><input type="radio" value="4" name="NOACs"> Apixaban<br></span>
                            <span class="checkboxcss"><input type="radio" value="5" name="NOACs"> Edoxaban<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="NOACs"> no<br></span>


                        </div>
                        <br>
                        <div class="select_section" id="section5_">
                            <h2>Family History</h2></span>
                            <!-- Family screening -->
                            <br>
                            <h3>Family screening record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Family_screening"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Family_screening"> no<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Family_screening"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="Family_screening"> none record<br></span>


                            <!-- Marriage record -->
                            <hr>
                            <h3>Marriage record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="Marriage"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="0" name="Marriage"> unmarried<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="Marriage"> married<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="Marriage"> none record<br></span>

                            <!-- Son record -->
                            <hr>
                            <h3>Son record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="son"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="son"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="son"> no<br></span>

                            <!-- Daughter record -->
                            <hr>
                            <h3>Daughter record</h3>
                            <span class="checkboxcss"><input type="radio" value="%" checked="checked" name="daughter"> all<br></span>
                            <span class="checkboxcss"><input type="radio" value="1" name="daughter"> yes<br></span>
                            <span class="checkboxcss"><input type="radio" value="2" name="daughter"> no<br></span>



                            <!-- Family_Note -->
                            <!--
                            <h3>Family Note</h3>
                            <input type="radio" value="%" checked="checked" name="Family_Note"> all<br>
                            <input type="radio" value="1" name="Family_Note"> HTN<br>
                            <input type="radio" value="2" name="Family_Note"> DM<br>
                            <input type="radio" value="3" name="Hyperlipidemia"> all<br>
                            <input type="radio" value="4" name="Thyroid problem"> all<br>
                            <input type="radio" value="5" name="Family_Note"> Allergy immunity<br>
                            <input type="radio" value="6" name="Family_Note"> tumor<br>
                            <input type="radio" value="7" name="Family_Note"> Stroke<br>
                            <input type="radio" value="8" name="Family_Note"> AMI<br>
                            <input type="radio" value="9" name="Family_Note"> CAD<br>
                            <input type="radio" value="10" name="Family_Note"> Arrhythmia<br>
                            <input type="radio" value="11" name="Family_Note"> SCD<br>
                            <input type="radio" value="12" name="Family_Note"> other family disease<br>
                            <input type="radio" value="0" name="Family_Note"> none record<br>
                          -->

                        </div>
                        <!--
                        <div class="select_section" id="section6_">

                            Sudden death emergency information<br>
                             Sudden death timeing
                            <h3>Sudden death timeing</h3>
                            <input type="radio" value="all" checked="checked" name="Sudden_death_timeing"> all<br>
                            <input type="radio" value="in" name="Sudden_death_timeing"> Sudden death in the hospital<br>
                            <input type="radio" value="before" name="Sudden_death_timeing"> Sudden death before arriving the hospital<br>
                            <input type="radio" value="unknown" name="Sudden_death_timeing"> unknown<br>
                        </div>
                      -->

                        <input type="submit" class="submit" value="submit" style="float:right;margin-right:5vw;">
                    </form>



                </div>
                */?>
            </div>
        </div>
</section>
<script>
    var i = 1;
    var current_page = 1;
    var block_page = [0, <?php echo $b1_page.",".$b2_page;?>];

    $(document).ready(function() {
        $(".container_program").hide();
        $(".rf-button_block").hide();
        $("#rf" + i).removeClass("rf-inactive").addClass("rf-active");
        $("#rfm" + i).removeClass("rf-inactive").addClass("rf-active");
        $(".rf_block" + i + "_0").fadeIn(200, "swing");
    });

    $(".rf-item").click(function() {
        current_page = 0;
        $("#rf" + i).removeClass("rf-active").addClass("rf-inactive");
        $("#rfm" + i).removeClass("rf-active").addClass("rf-inactive");

        i = $(this).data('num');
        $("#rfm" + i).removeClass("rf-inactive").addClass("rf-active");


        $(".container_program").fadeOut(200, "swing");
        $('.' + $(this).data('rel')).fadeIn(200, "swing");
    });
</script>
@endsection