@extends('frontend.layouts.master')
@section('title', 'Contact us')
@section('content')
@section('nav_Contactus', 'active')

<link href="{{ asset('css/contact.css') }}" rel="stylesheet">

<section>
    <div class="container-in">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <div class="contact_area_box" id="box1">
                        <h5 class="contact_area_title">
                            Wei-Chih Huang </h5>
                        <div class="contact_area_info">
                            <ul>
                                <li><i class="far fa-envelope"></i>
                                    <a href="mailto:loveariddle.bi96g@g2.nctu.edu.tw">loveariddle.bi96g@g2.nctu.edu.tw</a></li>
                                <li><i class="fas fa-globe-asia"></i></i>
                                    <span>PhD, Institute of Bioinformatics and Systems Biology, National Chiao-Tung University</span> </li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                    <div class="contact_area_box" id="box2">
                        <h5 class="contact_area_title">
                            Hsin-Tzu Huang </h5>
                        <div class="contact_area_info">
                            <ul>
                                <li><i class="far fa-envelope"></i>
                                    <a href="mailto:hsintzu@g2.nctu.edu.tw">hsintzu@g2.nctu.edu.tw</a></li>
                                <li><i class="fas fa-globe-asia"></i></i>
                                    <span> Doctoral candidate, Department of Biological Science and Technology, National Chiao-Tung University</span> </li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                    <div class="contact_area_box" id="box3">
                        <h5 class="contact_area_title">
                            Po-Yuan Chen </h5>
                        <div class="contact_area_info">
                            <ul>
                                <li><i class="far fa-envelope"></i>
                                    <a href="mailto:shepherd71c@gmail.com">shepherd71c@gmail.com</a></li>
                                <li><i class="fas fa-globe-asia"></i></i>
                                    <span> MS Student, Institute of Bioinformatics and Systems Biology, National Chiao-Tung University</span> </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="rightmenu" style="margin-left:0;margin-top:25%;">
                        <div class="righttop">
                            <a href="https://www.nctu.edu.tw/en">
                                <img src="img/1200px-NCTU_emblem.svg.png" alt="" style="display:inline;float:left;  padding-left:10px;">
                            </a>
                            <a href="http://isblab.life.nctu.edu.tw/">
                                <img src="img/CHSlogo.png" alt="" style="padding-right:10px;display:inline;float:right;margin-top:3vh;margin-left:1vw;">
                            </a>
                        </div>

                    </div>
                    <div>
                    </div>
                </div>
            </div>
</section>


@endsection