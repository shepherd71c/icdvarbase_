@extends('frontend.layouts.master')
@section('title', 'Search')
@section('content')
@section('nav_Search', 'active')

<section>
    <div class="container-in" style="padding-bottom:2%;">
        <div class="container-inin">
            <div class="contain_other_page" style="padding-bottom:5%;">
                <div class="rf-form_list_block">
                    <ul style="margin-bottom:0;    border-bottom: #535c3d 4px solid;">

                        <li>
                            <div class="rf-item" data-num="1" data-rel="rf_block1_0" style="cursor: auto; ">
                                <div id="rfm1" class="rf-active">{{$disease_name}}</div>
                            </div>
                        </li>
                    </ul>

                    <div class="container_program rf_block1_0">
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th title="Field #2" style="width:30%;background-color:#CAB44F ;">Disease-association gene</th>
                                    <th title="Field #3" style="width:40%;background-color:#CAB44F ;">Variant</th>
                                    <th title="Field #4" style="width:30%;background-color:#CAB44F ;">Case</th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach($disease_browser1 as $key => $data)
                                <tr>
                                    <th>{{$data->gene}}</th>
                                    <th>{{$data->variants}}</th>
                                    <th>
                                        <form action="{{url('Browse_case')}}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="name" value={{$data->Barcode}}>
                                            <button type="submit" name="button">{{$data->Barcode}}</button>
                                        </form>
                                    </th>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <table class="table table-bordered table-hover table-condensed" style="margin-top:5vh;">
                            <thead>
                                <tr>
                                    <th title="Field #2" style="width:30%;background-color:#ABC3BF ;">ICC/SCD-association gene</th>
                                    <th title="Field #3" style="width:40%;background-color:#ABC3BF ;">Variant</th>
                                    <th title="Field #4" style="width:30%;background-color:#ABC3BF ;">Case</th>

                                </tr>
                            </thead>
                            <tbody>

                                @foreach($disease_browser2 as $key => $data)
                                <tr>
                                    <th>{{$data->gene}}</th>
                                    <th>{{$data->variants}}</th>
                                    <th>
                                        <form action="{{url('Browse_case')}}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="name" value={{$data->Barcode}}>
                                            <button type="submit" name="button">{{$data->Barcode}}</button>
                                        </form>
                                    </th>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>


                    </div>

                </div>
            </div>
        </div>
</section>

@endsection