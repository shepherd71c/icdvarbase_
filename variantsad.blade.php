@extends('frontend.layouts.master')
@section('title', 'SVAD » Variants')
@section('content')


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css" />



<div class="variants_ad" style="min-height:78.5vh;">

    <h2>Accession ID:
        @php
        $id = str_pad($id,4,"0", STR_PAD_LEFT);
        echo "SVAD$id";
        @endphp</h2>
        </br>
        <h3>Information retrieved from article</h3>

        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#EAE2B7 ; width: 8vw;">Pubmed ID</th>
                    <th title="Field #2" style="background-color:#EAE2B7 ; width: 6vw;">Disease</th>
                    <th title="Field #3" style="background-color:#EAE2B7 ; width: 8vw;">Race</th>
                    <th title="Field #4" style="background-color:#EAE2B7 ; width: 6vw;">Patient No.</th>
                    <th title="Field #5" style="background-color:#EAE2B7 ; width: 6vw;">Control No.</th>


                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_record as $key => $data)
                <tr>
                    <td><a href="https://www.ncbi.nlm.nih.gov/pubmed/?term={{$data->PMID}}">{{$data->PMID}}</a>
                    <td>
                        @php

                        if(is_null($data->Disease)){
                        echo "-";
                        }
                        else {
                        echo $data->Disease;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->Race)){
                        echo "-";
                        }
                        else {
                        echo $data->Race;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->TPNUM)){
                        echo "-";
                        }
                        else {
                        echo $data->TPNUM;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->CPNUM)){
                        echo "-";
                        }
                        else {
                        echo $data->CPNUM;
                        }

                        @endphp</td>

                </tr>
                @endforeach
            </tbody>
        </table>
        </br>

        <h3>Variant information</h3>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#C6E4EC ; width: 8%;">Gene</th>
                    <th title="Field #2" style="background-color:#C6E4EC ; width: 8%;">Chromosome</th>
                    <th title="Field #3" style="background-color:#C6E4EC ; width: 8%;">Location</th>
                    <th title="Field #4" style="background-color:#C6E4EC ; width: 8%;">REF sequence</th>
                    <th title="Field #5" style="background-color:#C6E4EC ; width: 8%;">ALT sequence</th>
                    <th title="Field #6" style="background-color:#C6E4EC ; width: 8%;">SNP ID</th>
                    <th title="Field #7" style="background-color:#C6E4EC ; width: 8%;">Transcript_ID</th>
                    <th title="Field #8" style="background-color:#C6E4EC ; width: 8%;">DNA change</th>
                    <th title="Field #9" style="background-color:#C6E4EC ; width: 8%;">Protein change</th>
                    <th title="Field #10" style="background-color:#C6E4EC ; width: 8%;">Alteration</th>




                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_record as $key => $data)
                <tr>
                    <td>
                        @php

                        if(is_null($data->Gene)){
                        echo "-";
                        }
                        else {
                        echo $data->Gene;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->Chromosome)){
                        echo "-";
                        }
                        else {
                        echo $data->Chromosome;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->Location)){
                        echo "-";
                        }
                        else {
                        echo $data->Location;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->ref)){
                        echo "-";
                        }
                        else {
                        echo $data->ref;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->alt)){
                        echo "-";
                        }
                        else {
                        echo $data->alt;
                        }

                        @endphp</td>
                    <td>
                        @php


                        if(is_null($data->SNP_ID)){
                        echo "-";
                        }
                        else {

                        @endphp
                        <a href="https://www.ncbi.nlm.nih.gov/snp/{{$data->SNP_ID}}">{{$data->SNP_ID}}

                            @php

                            }

                            @endphp</a>
                    </td>
                    <td>
                        @php

                        if(is_null($data->Transcript_ID)){
                        echo "-";
                        }
                        else {
                        echo $data->Transcript_ID;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->HGVS_DNA)){
                        echo "-";
                        }
                        else {
                        echo $data->HGVS_DNA;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->HGVS_Protein2)){
                        echo "-";
                        }
                        else {
                        echo $data->HGVS_Protein2;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->Pro_Alter)){
                        echo "-";
                        }
                        else {
                        echo $data->Pro_Alter;
                        }

                        @endphp</td>


                </tr>
                @endforeach
            </tbody>
        </table>
        </br>

        <h3>Reported classification and prediction</h3>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#C6E4EC ; width: 14vw;">ACMG classification (Varsome)</th>
                    <th title="Field #2" style="background-color:#C6E4EC ; width: 14vw;">ClinVar</th>
                    <th title="Field #3" style="background-color:#C6E4EC ; width: 10vw;">CADD:SIFT</th>
                    <th title="Field #4" style="background-color:#C6E4EC ; width: 7vw;">SIFT score</th>
                    <th title="Field #5" style="background-color:#C6E4EC ; width: 10vw;">CADD:PolyPhen</th>
                    <th title="Field #6" style="background-color:#C6E4EC ; width: 12vw;">PolyPhen score</th>



                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_record as $key => $data)
                <tr>
                    <td>
                        @php

                        if(is_null($data->ACMG_Varsome)){
                        echo "-";
                        }
                        else {
                        echo $data->ACMG_Varsome;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->ACMG_Varsome)){
                        echo "-";
                        }
                        else {
                        @endphp
                        <a href="https://www.ncbi.nlm.nih.gov/clinvar/variation/{{$data->ClinVar_ID}}/">{{$data->ClinVar_class}}
                        </a>
                        @php

                        }

                        @endphp
                    </td>
                    <td>
                        @php

                        if(is_null($data->CADD_SIFT)){
                        echo "-";
                        }
                        else {
                        echo $data->CADD_SIFT;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->CADD_SIFTval)){
                        echo "-";
                        }
                        else {
                        echo $data->CADD_SIFTval;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->CADD_PolyPhen)){
                        echo "-";
                        }
                        else {
                        echo $data->CADD_PolyPhen;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->CADD_PolyPhenval)){
                        echo "-";
                        }
                        else {
                        echo $data->CADD_PolyPhenval;
                        }

                        @endphp</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </br>

        <h3>Allele frequency in diverse populations</h3>

        <h6> - dbsnp</h6>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#E7BEB6 ; width: 10vw;">REF sequence</th>
                    <th title="Field #2" style="background-color:#E7BEB6 ; width: 10vw;">ALT sequence</th>
                    <th title="Field #3" style="background-color:#E7BEB6 ; width: 5vw;">GMAF</th>

                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_dbsnp as $key => $data)
                <tr>
                    <td>
                        @php

                        if(is_null($data->refbase)){
                        echo "-";
                        }
                        else {
                        echo $data->refbase;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->altbase)){
                        echo "-";
                        }
                        else {
                        echo $data->altbase;
                        }

                        @endphp</td>
                    <td>
                        <?php $a = explode(":",$data->GMAF);
                              $a1 = $a[0];
                              if($a1 == 0){
                              echo "-";
                              }
                              elseif (is_null($a)) {
                              echo "-";
                              }
                              else {
                                $a2 = $a[1];
                                echo  round($a2,4);

                              }

                                  ?>
                    </td>


                </tr>
                @endforeach
            </tbody>
        </table>
        </br>
        <h6> - 1000 genome</h6>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#E7BEB6 ; width: 8vw;">AFR</th>
                    <th title="Field #2" style="background-color:#E7BEB6 ; width: 8vw;">AMR</th>
                    <th title="Field #3" style="background-color:#E7BEB6 ; width: 8vw;">SAS</th>
                    <th title="Field #4" style="background-color:#E7BEB6 ; width: 8vw;">EAS</th>
                    <th title="Field #5" style="background-color:#E7BEB6 ; width: 8vw;">EUR</th>
                    <th title="Field #6" style="background-color:#E7BEB6 ; width: 8vw;">AF</th>
                    <th title="Field #7" style="background-color:#E7BEB6 ; width: 8vw;">NS</th>


                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_1000g as $key => $data)
                <tr>
                    <td>
                        @php

                        if(is_null($data->AFR_AF)){
                        echo "-";
                        }
                        else {
                        echo $data->AFR_AF;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->AMR_AF)){
                        echo "-";
                        }
                        else {
                        echo $data->AMR_AF;
                        }

                        @endphp
                    </td>
                    <td>
                        @php

                        if(is_null($data->SAS_AF)){
                        echo "-";
                        }
                        else {
                        echo $data->SAS_AF;
                        }

                        @endphp
                    </td>
                    <td>
                        @php

                        if(is_null($data->EAS_AF)){
                        echo "-";
                        }
                        else {
                        echo $data->EAS_AF;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->EUR_AF)){
                        echo "-";
                        }
                        else {
                        echo $data->EUR_AF;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->AF)){
                        echo "-";
                        }
                        else {
                        echo $data->AF;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->NS)){
                        echo "-";
                        }
                        else {
                        echo $data->NS;
                        }

                        @endphp</td>

                </tr>
                @endforeach
            </tbody>
        </table>
        </br>
        <h6> - The Exome Aggregation Consortium (ExAC)</h6>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#E7BEB6 ; width: 8vw;">AFR_AF</th>
                    <th title="Field #2" style="background-color:#E7BEB6 ; width: 8vw;">AMR_AF</th>
                    <th title="Field #3" style="background-color:#E7BEB6 ; width: 8vw;">SAS_AF</th>
                    <th title="Field #4" style="background-color:#E7BEB6 ; width: 8vw;">EAS_AF</th>
                    <th title="Field #5" style="background-color:#E7BEB6 ; width: 8vw;">FIN_AF</th>
                    <th title="Field #6" style="background-color:#E7BEB6 ; width: 8vw;">AF</th>

                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_exac as $key => $data)
                <tr>
                    <td>
                        @php
                        if(is_null($data->AFR_AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->AFR_AF,4);
                        }
                        @endphp</td>
                    <td>
                        @php
                        if(is_null($data->AMR_AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->AMR_AF,4);
                        }
                        @endphp</td>
                    <td>
                        @php
                        if(is_null($data->SAS_AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->SAS_AF,4);
                        }
                        @endphp</td>
                    <td>
                        @php
                        if(is_null($data->EAS_AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->EAS_AF,4);
                        }
                        @endphp</td>
                    <td>
                        @php
                        if(is_null($data->FIN_AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->FIN_AF,4);
                        }
                        @endphp</td>
                    <td>
                        @php
                        if(is_null($data->AF)){
                        echo "-";
                        }
                        else {
                        echo round($data->AF,4);
                        }
                        @endphp</td>

                </tr>
                @endforeach
            </tbody>
        </table>
        </br>
        <h6> - TaiwanBiobank</h6>
        <table class="table ad" style="margin-bottom: 1vh;overflow-x: scroll!important;width:auto;">
            <thead>
                <tr>
                    <!-- SUD_DB -->
                    <th title="Field #1" style="background-color:#E7BEB6 ; width: 8vw;">MAF</th>
                    <th title="Field #2" style="background-color:#E7BEB6 ; width: 8vw;">Genotype 1</th>
                    <th title="Field #3" style="background-color:#E7BEB6 ; width: 16vw;">Genotype 1 frequency</th>
                    <th title="Field #4" style="background-color:#E7BEB6 ; width: 8vw;">Genotype 2</th>
                    <th title="Field #5" style="background-color:#E7BEB6 ; width: 16vw;">Genotype 2 frequency</th>
                    <th title="Field #6" style="background-color:#E7BEB6 ; width: 8vw;">Genotype 3</th>
                    <th title="Field #7" style="background-color:#E7BEB6 ; width: 16vw;">Genotype 3 frequency</th>
                </tr>
            </thead>
            <tbody>
                @foreach($variant_table_TB as $key => $data)
                <tr>
                    <td>
                        @php

                        if(is_null($data->MAF)){
                        echo "-";
                        }
                        else {
                        echo $data->MAF;
                        }

                        @endphp
                    </td>
                    <td>
                        @php

                        if(is_null($data->GT1)){
                        echo "-";
                        }
                        else {
                        echo $data->GT1;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->GT1_freq)){
                        echo "-";
                        }
                        else {
                        echo $data->GT1_freq;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->GT2)){
                        echo "-";
                        }
                        else {
                        echo $data->GT2;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->GT2_freq)){
                        echo "-";
                        }
                        else {
                        echo $data->GT2_freq;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->GT3)){
                        echo "-";
                        }
                        else {
                        echo $data->GT3;
                        }

                        @endphp</td>
                    <td>
                        @php

                        if(is_null($data->GT3_freq)){
                        echo "-";
                        }
                        else {
                        echo $data->GT3_freq;
                        }

                        @endphp</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        </br>
        </br>
        </br>

        </br>

        <script src="vendor/jquery/jquery.min.js"></script>

</div>

@endsection